<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLtcJourneyDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('ltc_claim_journey_detail', function (Blueprint $table) {
            $table->string('arrival_dat')->nullable();
            $table->string('passnger_no')->nullable();
            $table->string('source_stat')->nullable();
            $table->string('dest_stat')->nullable();
            $table->string('dept_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
