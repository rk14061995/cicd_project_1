<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmployeeLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employee_leave',function(Blueprint $table){
            $table->id()->autoIncrement();
            $table->string('emp_id')->nullable();
            $table->string('empInchgId')->nullable();
            $table->string('category')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->enum('req_status',['1','0','2'])->default(2)->comment('1:Approved, 0: Pending, 2: Rejected');
            $table->string('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
