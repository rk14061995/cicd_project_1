<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SalaryEntPercentage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Array
        // (
        // [hra_percent] => 27
        // [da_percent] => 38
        // [tptValue] => 3600
        // [tptPerValue] => 10
        // [tpt_amount] => 4968
        // )
        Schema::create('emp_sal_percentage',function(Blueprint $table){
            $table->id()->autoIncrement();
            $table->string('hra_percent')->nullable();
            $table->string('da_percent')->nullable();
            $table->string('tptValue')->nullable();
            $table->string('tpt_amount')->nullable();
            $table->string('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
