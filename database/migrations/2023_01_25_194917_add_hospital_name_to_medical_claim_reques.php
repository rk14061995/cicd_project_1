<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHospitalNameToMedicalClaimReques extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_claim_reques', function (Blueprint $table) {
            $table->string('hospital_name')->nullable();
            $table->string('dependent_cghs_id')->nullable();
            $table->string('dependent_adhar_id')->nullable();
            $table->string('prd_of_treatment')->nullable();
            $table->string('prd_of_claim')->nullable();
            $table->string('claimed_on')->nullable();
            $table->string('approved_on')->nullable();
            $table->string('dv_no')->nullable();
            $table->string('cheq_or_draft_no')->nullable();
            $table->string('remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_claim_reques', function (Blueprint $table) {
            //
        });
    }
}
