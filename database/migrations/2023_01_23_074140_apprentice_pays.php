<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ApprenticePays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('apprentice_pays',function(Blueprint $table){
            $table->id()->autoIncrement();
            $table->string('reg_no')->nullable();
            $table->string('name')->nullable();
            $table->string('fname')->nullable();
            $table->string('dob')->nullable();
            $table->string('trade')->nullable();
            $table->string('doa')->nullable();
            $table->string('pdays')->nullable();
            $table->string('day_in_month')->nullable();
            $table->string('stipend')->nullable();
            $table->string('net_pay')->nullable();
            $table->string('remarks')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
