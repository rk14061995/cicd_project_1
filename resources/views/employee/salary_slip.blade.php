@extends('employee.layouts.main')
@section('main')
    <!-- end topbar -->
    <!-- dashboard inner -->
    <div class="midde_cont mt-2">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="col text-left">
                            
                            <label class="font-weight-bold"> Employee Salary Slip</label>
                               
                        </div>
                        
                    </div>
                </div>
            </div>
            
            
            <div class="row my-1">
                {{-- <style>
                    .tdd{
                        width:20px;
                        height: 20px;
                    }
                    input {
                        border-top-style: hidden;
                        border-right-style: hidden;
                        border-left-style: hidden;
                        border-bottom-style: groove;
                        /* background-color: #eee; */
                        }

                        .no-outline:focus {
                        outline: none;
                        }
                </style> --}}
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" role="tablist">
                               
                                
                                <li class="nav-item  ">
                                    <a class="nav-link active" data-toggle="tab" href="#emp_sal_slip" role="tab">Salary Slip </a>
                                </li>
                                


                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                               
                                <div class="tab-pane active" id="emp_sal_slip" role="tabpanel">
                                    <span class="text-danger">Note: Previous Month Generated Salary</span>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('salray-slip')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="salray-slip">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            @if (!empty($salary->empdetails))
                                                            <table class="w-100" border="1"  >
                                                                <tr>
                                                                    <th colspan="3"><u>STATION WORKSHOP EME, DELHI CANTT-10<br><br></u></th>
                                                                </tr>
                                                            
                                                                <tr>
                                                                  @php
                                                                //   echo $salary->for_month_year;
                                                                    //   $sl=explode('-',$salary->for_month_year);

                                                                  @endphp  
                                                                    <td colspan="2">Payslip for the month : {{!empty($salary->for_month_year) ? $salary->for_month_year : ''}}<br>Name: {{$salary->empdetails->name}}<br>
                                                                    DOB: {{$salary->empdetails->dob}}</td>
                                                                    <td>T.No : {{$salary->empdetails->t_no}}<br>Trade : {{$salary->empdetails->trade}},<br>DOA : {{$salary->empdetails->doa}}</td>
                                                                    <td rowspan="2">EOL(D)
                                                                        <hr style="border: 2px solid black;">EOL(D)
                                                                    </td>
                                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td style="text-align: center;">ENTITLEMENTS</td>
                                                                    <td style="text-align: center;">DEDUCTION</td>
                                                                    <td style="text-align: center;">TABLE DEDUCTION</td>
                                                                    
                                                                </tr>
                                                            
                                                                <tr>
                                                                    <td rowspan="3">
                                                                        BP : {{$salary->basic_pay}}<br>DA: {{$salary->da}}<br>HRA : {{$salary->hra}} <br>TPT : {{$salary->tpt}}<br>WA : {{$salary->wa}}<br>SPL PAY : {{$salary->pers_pay}}
                                                                        <br>MISC : {{$salary->misc}} 
                                                                    </td>
                                                                    <td rowspan="3">
                                                                        AWL(D) : 0 <br> AWL(H) : 0<br>GPF : {{$salary->gpf_ref}}<br>CGEIS : {{$salary->cgeis}}<br>CGHS : {{$salary->cghs}} <br>FA : {{$salary->f_adv}}<br>Rent Rec : {{$salary->rent_rec}}<br>MISC : {{$salary->misc_ded}}
                                                                    </td>
                                                                    <td rowspan="3">
                                                                        SOCIETY : {{$tbDed->society}}<br>CWF : {{$tbDed->cwf}} <br>LIC : {{$tbDed->lic}}<br>MISC : {{$tbDed->misc}}
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                   
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                    
                                                                <tr>
                                                              <td>TOTAL PAY : {{$t1=$salary->basic_pay + $salary->da +$salary->hra + $salary->tpt+ $salary->pers_pay + $salary->wa + $salary->misc}} </td>
                                                              <td>TOTALDED: {{$t2=$salary->gpf_ref + $salary->cgeis +$salary->cghs + $salary->f_adv+ $salary->rent_rec + $salary->misc_ded}}</td>
                                                              <td colspan="2">TOTAL TABLE GER: {{$t3=$tbDed->society + $tbDed->cwf +$tbDed->lic + $tbDed->misc}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">TOTAL PAYABLE AFTER DER : {{$t4=$t1-$t2}}</td>
                                                                    <td colspan="2">NETPAY : {{$t4-$t3}}</td>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td colspan="4">Remarks:<br><br></td>
                                                                </tr>
                                                    
                                                            </table>
                                                            @else
                                                                <div class="alert-danger">
                                                                    No Salary Slip Found
                                                                </div>
                                                            @endif
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
