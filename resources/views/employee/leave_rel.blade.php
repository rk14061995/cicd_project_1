@extends('employee.layouts.main') 
@section('main')
<!-- end topbar -->
<!-- dashboard inner -->
<div class="midde_cont py-2">
    <div class="container-fluid">
        <div class="row d-none">
            <div class="col text-center">
                <div class="card">
                    <div class="card-body">
                    <p class="font-weight-bold ">Leave Request : <span class="text-info"> Pending</span>    </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-1 d-none">
            <div class="col text-center">
                <div class="card">
                    <div class="card-body">
                        <p class="font-weight-bold ">Total Leaves Remaining (Current Year) : <span class="text-info"> 60</span>    </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-none">
            <div class="col text-center">
                <div class="card">
                    <div class="card-body">
                        <p class="font-weight-bold ">Total Leaves Remaining (Lifetime) : <span class="text-info"> 600</span>    </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-1">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h5>Leave Request Form</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item ">
                                <a class="nav-link " data-toggle="tab" href="#emp_lv_lst" role="tab">Pending Requests</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link " data-toggle="tab" href="#emp_lv_lst_app" role="tab">Approved Requests</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link active" data-toggle="tab" href="#app_lv" role="tab">Apply For Leave</a>
                            </li>
                            
                            
                        </ul><!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane " id="emp_lv_lst_app" role="tabpanel">
                                <table class="table table-bordered myTable">
                                    <thead>
                                        <th>S.No</th>
                                        <th>Emp. Id</th>
                                        <th>Emp Name</th>
                                        <th>Incharge</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Req Status</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i=1;
                                        @endphp
                                        @foreach ($leave as $d)
                                            @if ($d->req_status == 1)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$d->emp_id}}</td>
                                                    <td>{{$d->empdetails->name}}</td>
                                                    <td>{{$d->inchDtl->name}}</td>
                                                    <td>{{$d->from}}</td>
                                                    <td>{{$d->to}}</td>
                                                    <td>
                                                        @if ($d->req_status==1)
                                                            <span class="text-success">Approved</span>
                                                        @elseif($d->req_status==2)
                                                            <span class="text-info">Pending</span>
                                                        @else
                                                            <span class="text-danger">Rejected</span>
                                                        @endif
                                                        
                                                        
                                                        
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0)" class="text-success " data-id="{{$d->id}}"><i
                                                            class="fa fa-edit" aria-hidden="true"  ></i></a>
                                                        <a href="javascript:void(0)" class="text-danger " data-id="{{$d->id}}"><i
                                                                class="fa fa-trash" aria-hidden="true"  ></i></a>
                                                    </td>
                                                </tr>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </tbody>
                                   
                                </table>
                            </div>
                           
                            <div class="tab-pane active" id="app_lv" role="tabpanel">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <form id="leave-emp-Id" action={{route('leave-management')}} method="get">
                                                    <label>Employee Id</label>
                                                    <select class="form-control empSearch" style="width:100%"  name="emp" id="lv_emp_id">
                                                        <option value="0">Select</option>
                                                        @foreach ($data as $user)
                                                            @if (!empty($empId) && $empId!=null && $empId==$user->emp_id)
                                                                <option value="{{$user->emp_id}}" selected>{{$user->name}} ({{$user->emp_id}})</option>
                                                            @else
                                                                <option value="{{$user->emp_id}}">{{$user->name}} ({{$user->emp_id}})</option>
                                                            @endif
                                                          
                                                        @endforeach     
                                                    </select>
                                                </form>
                                                
                                            </div>
                                        </div>
                                        <div class="row mt-5">
                                            <div class="col">
                                                <label>Employee Leave Details</label>
                                                <hr>
                                                <table class="table table-bordered " >
                                                    <thead>
                                                      
                                                        <th>Category Name</th>
                                                        <th class="text-center">Days</th>
                                                        <th class="text-center">Available Leaves</th>
                                                        
                                                    </thead>
                                                    <tbody>
                                                       @php
                                                           $ttlLvs=0;
                                                           $ttlAvlLvs=0;
                                                       @endphp
                                                        @foreach ($category as $cate)
                                                           @php
                                                               $ttlLvs+=$cate['days'];
                                                               $ttlAvlLvs+=$cate['employeeLeaves'];
                                                           @endphp
                                                            <tr>
                                                             
                                                                <td>{{$cate['category']}}</td>
                                                                <td class="text-center">{{$cate['days']}}</td>
                                                                <td class="text-center">{{$cate['days'] - $cate['employeeLeaves']}} </td>
                                                                
                                                            </tr>
                                                            
                                                        @endforeach
                                                        
                                                    </tbody>
        
                                                </table>
                                                <table class="table table-bordered">
                                                    <tr class="text-center">
                                                        <th>Total Leaves: {{$ttlLvs}}</th>
                                                        <th>Remaining Leaves: {{$ttlLvs-$ttlAvlLvs}}</th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-8">
                                                <form id="apply-for-leave">
                                                    <div class="row">
                                                        <div class="col">
                                                            <label>Apply For Leave</label>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <input type="hidden" name="empId" value="{{!empty($empId) ? $empId : ''}}">
                                                        <div class="col">
                                                            <label>Group Incharge</label>
                                                            <select class="form-control empSearch" style="width:100%" name="empInchgId">
                                                                <option value="0">Select</option>
                                                                @foreach ($data as $user)
                                                                    <option value="{{$user->emp_id}}">{{$user->name}} ({{$user->emp_id}})</option>
                                                                @endforeach     
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label>Leave Category</label>
                                                            <select class="form-control" name="reserved_for">
                                                                <option value="0">Select</option>
                                                                @foreach ($lvCategory as $cate)
                                                                    <option value="{{$cate->category_name}}">{{$cate->category_name}}</option>
                                                                @endforeach     
                                                            </select>
                                                           
                                                        </div>  
                                                    </div>
                                                    <div class="row">
                                                        
                                                        <div class="col">
                                                            <label>From</label>
                                                            <input type="date" name="form" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <label>To</label>
                                                            <input type="date" name="to" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <hr>
                                                            <input type="submit" value="Submit" class="btn btn-success">
                                                        </div>
                                                        
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane " id="emp_lv_lst" role="tabpanel">
                                <table class="table table-bordered myTable">
                                    <thead>
                                        <th>S.No</th>
                                        <th>Emp. Id</th>
                                        <th>Emp Name</th>
                                        <th>Incharge</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Req Status</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i=1;
                                        @endphp
                                        @foreach ($leave as $d)
                                            @if ($d->req_status ==2)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$d->emp_id}}</td>
                                                    <td>{{$d->empdetails->name}}</td>
                                                    <td>{{$d->inchDtl->name}}</td>
                                                    <td>{{$d->from}}</td>
                                                    <td>{{$d->to}}</td>
                                                    <td>
                                                        @if ($d->req_status==1)
                                                            <span class="text-success">Approved</span>
                                                        @elseif($d->req_status==2)
                                                            <span class="text-info">Pending</span>
                                                        @else
                                                            <span class="text-danger">Rejected</span>
                                                        @endif
                                                        
                                                        
                                                        
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0)" class="text-success " data-id="{{$d->id}}"><i
                                                            class="fa fa-edit" aria-hidden="true"  ></i></a>
                                                        <a href="javascript:void(0)" class="text-danger " data-id="{{$d->id}}"><i
                                                                class="fa fa-trash" aria-hidden="true"  ></i></a>
                                                    </td>
                                                </tr>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </tbody>
                                   
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    

       
    </div>
</div>
@endsection