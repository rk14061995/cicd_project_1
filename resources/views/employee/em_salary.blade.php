@extends('employee.layouts.main')
@section('main')
    <!-- end topbar -->
    <!-- dashboard inner -->
    <div class="midde_cont mt-2">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="col text-left">
                            
                            <label class="font-weight-bold"> Employee Pay Roll</label>
                               
                        </div>
                        <div class="col text-right">
                            <a href="javascript:void(0)" id="generateEmpSalary" class="btn btn-info">Generate Salary</a>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row my-1">
                {{-- <style>
                    .tdd{
                        width:20px;
                        height: 20px;
                    }
                    input {
                        border-top-style: hidden;
                        border-right-style: hidden;
                        border-left-style: hidden;
                        border-bottom-style: groove;
                        /* background-color: #eee; */
                        }

                        .no-outline:focus {
                        outline: none;
                        }
                </style> --}}
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#empl_list" role="tab">Employee Salary</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" data-toggle="tab" href="#emp_pay_roll" role="tab">Generated Salary</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" data-toggle="tab" href="#emp_pay_roll_days" role="tab">Working Days</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" data-toggle="tab" href="#comm_pay_roll_nps" role="tab">Commulative NPS</a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" data-toggle="tab" href="#comm_pay_roll" role="tab">Commulative</a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" data-toggle="tab" href="#arrear_da" role="tab">Arrear DA</a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" data-toggle="tab" href="#arrear_da_nps" role="tab">Arrear DA(NPS) </a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" data-toggle="tab" href="#bill_pay" role="tab">Bill Pay </a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" data-toggle="tab" href="#emp_sal_slip" role="tab">Salary Slip </a>
                                </li>
                                


                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="empl_list" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item ">
                                                    <a class="nav-link active" data-toggle="tab" href="#empl_sal_slip" role="tab">Salary Slip</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link " data-toggle="tab" href="#empl_ent" role="tab">Entitlements</a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link" data-toggle="tab" href="#emp_deduct" role="tab">Deductions</a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link" data-toggle="tab" href="#emp_tbl_ddct" role="tab">Table Deductions</a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link" data-toggle="tab" href="#empl_net_pay" role="tab">Pay To Bank</a>
                                                </li>
                                            </ul><!-- Tab panes -->
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="empl_sal_slip" role="tabpanel">
                                                    <table class="table table-bordered myTable " style="font-size: 11px"> 
                                                        <thead>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Employee Name</th>
                                                            <th class="text-center">Employee Id</th>
                                                            <th class="text-center">Action</th>
        
                                                        </thead>
        
                                                        <tbody class="text-center">
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($employee as $emp)
                                                                <tr>
                                                                    <td>{{$i}}</td>
                                                                    <td>{{$emp->name}}</td>
                                                                    <td>{{$emp->emp_id}}</td>
                                                                    
                                                                    <td>
                                                                        <a href="{{route('view-generated-salary-slip',$emp->emp_id)}}" class="text-primary">View Salaray Slip</a>
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
        
                                                        </tbody>
        
        
                                                    </table>
                                                </div>
                                                <div class="tab-pane " id="empl_ent" role="tabpanel">
                                                    <table class="table table-bordered myTable " style="font-size: 11px"> 
                                                        <thead>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Employee Name</th>
                                                            <th class="text-center">Employee Id</th>
                                                            <th class="text-center">Basic Pay</th>
                                                            <th class="text-center">DA</th>
                                                            <th class="text-center">HRA</th>
                                                            <th class="text-center">TPT</th>
                                                            <th class="text-center">Pers Pay</th>
                                                            <th class="text-center">Govt Perks</th>
                                                            <th class="text-center">WA</th>
                                                            <th class="text-center">MISC</th>
                                                            <th class="text-center">Total</th>
                                                            <th class="text-center">Action</th>
        
                                                        </thead>
        
                                                        <tbody class="text-center">
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($employee as $emp)
                                                                <tr>
                                                                    <td>{{$i}}</td>
                                                                    <td><a href="javascript:void(0)" class="text-primary">{{$emp->name}}</a></td>
                                                                    <td><a href="javascript:void(0)" class="text-primary">{{$emp->emp_id}}</a></td>
                                                                    <td><input type="text" value="{{ $emp->basic_pay}}" id="ent_bp_{{$emp->emp_id}}" name="basic_pay" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->ta_da}}" id="ent_da_{{$emp->emp_id}}" name="ta_da" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->hra}}" id="ent_hra_{{$emp->emp_id}}" name="hra" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->tpt}}" id="ent_tpt_{{$emp->emp_id}}" name="tpt" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->pers_pay}}" id="ent_pp_{{$emp->emp_id}}" name="pers_pay" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->govt_perks}}" id="ent_gvt_{{$emp->emp_id}}" name="govt_perks" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->wa}}" name="govt_perks" id="ent_wa_{{$emp->emp_id}}"style="width: 69px;font-size: 12px;text-align: center;"></td>  
                                                                    <td><input type="text" value="{{ $emp->misc}}" id="ent_misc_{{$emp->emp_id}}" name="govt_perks" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->basic_pay +$emp->ta_da +$emp->hra +$emp->tpt +$emp->pers_pay +$emp->govt_perks +$emp->wa }}" name="govt_perks" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td>
                                                                        <a href="javascript:void(0)" class="btn btn-info updateEmpSal" data-empid="{{$emp->emp_id}}">Update</a>
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
        
                                                        </tbody>
        
        
                                                    </table>
                                                </div>
                                                <div class="tab-pane " id="emp_deduct" role="tabpanel">
                                                    <table class="table table-bordered myTable " style="font-size: 11px">
                                                        <thead>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Employee Name</th>
                                                            {{-- <th class="text-center">Employee Id</th>    --}}
                                                            <th class="text-center">EOL(H)</th>
                                                            <th class="text-center">EOL(D)</th>
                                                            <th class="text-center">GPF Ref</th>
                                                            <th class="text-center">NPS</th>
                                                            <th class="text-center">CGEIS</th>
                                                            <th class="text-center">CGHS Pay</th>
                                                            <th class="text-center">F Adv</th>
                                                            <th class="text-center">Rent Rec</th>
                                                            <th class="text-center">Misc</th>
                                                            <th class="text-center">Tax Ded</th>
                                                            <th class="text-center">Total Ded</th>
                                                            <th class="text-center">Action</th>
        
                                                        </thead>
        
                                                        <tbody class="text-center">
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($employee as $emp)
                                                                <tr>
                                                                    <td>{{$i}}</td>
                                                                    <td><a href="javascript:void(0)" >{{$emp->name}} <span style="font-size: 10px" class="text-danger">({{$emp->emp_id}})</span></a></td>
                                                                    {{-- <td><a href="javascript:void(0)" class="text-primary"></a></td> --}}
                                                                    <td><input type="text" value="0" id="eolh_ded" name="basic_pay" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="0" id="eold_ded" name="ta_da" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->gpf_ref}}" id="gpfref_ded_{{$emp->emp_id}}" name="hra" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->nps_deduction}}" id="nps_ded_{{$emp->emp_id}}" name="govt_perks" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->cgeis}}" id="cgeis_ded_{{$emp->emp_id}}" name="pers_pay" style="width: 30px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->cghs}}" id="cghs_ded_{{$emp->emp_id}}" name="govt_perks" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->f_adv}}" id="f_adv_ded_{{$emp->emp_id}}" name="govt_perks" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->rent_rec}}" id="rent_rec_ded_{{$emp->emp_id}}" name="govt_perks" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ !empty($emp->misc) ?   $emp->misc :    0}}" id="misc_ded_{{$emp->emp_id}}" name="govt_perks" style="width: 20px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->tax_deduction}}" id="tax_ded_{{$emp->emp_id}}" name="govt_perks" style="width: 45px;font-size: 11px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{$emp->gpf_ref+$emp->cgeis+$emp->cghs+$emp->f_adv+$emp->rent_rec+$emp->tax_deduction+$emp->nps_deduction+$emp->misc}}" name="govt_perks" style="width: 45px;font-size: 12px;text-align: center;"></td>  
                                                                    
                                                                    <td>
                                                                        <a href="javascript:void(0)" class="btn btn-info updateDeductions" data-empid="{{$emp->emp_id}}"  >Update</a>
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
        
                                                        </tbody>
        
        
                                                    </table>
                                                    
                                                </div>
                                                <div class="tab-pane " id="emp_tbl_ddct" role="tabpanel">
                                                    <table class="table table-bordered myTable " style="font-size: 11px">
                                                        <thead>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Employee Name</th>
                                                            <th class="text-center">Employee Id</th>
                                                            <th class="text-center">Society</th>
                                                            <th class="text-center">CWF</th>
                                                            <th class="text-center">LIC</th>
                                                            <th class="text-center">MISC</th>
                                                            <th class="text-center">Total</th>
                                                            <th class="text-center">Action</th>
        
                                                        </thead>
        
                                                        <tbody class="text-center">
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($employee as $emp)
                                                                <tr>
                                                                    <td>{{$i}}</td>
                                                                    <td><a href="javascript:void(0)" class="text-primary">{{$emp->name}}</a></td>
                                                                    <td><a href="javascript:void(0)" class="text-primary">{{$emp->emp_id}}</a></td>
                                                                    <td><input type="text" value="{{ $emp->society}}" id="tbl_society_{{$emp->emp_id}}" name="basic_pay" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->cwf}}" id="tbl_cwf_{{$emp->emp_id}}" name="ta_da" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->lic}}" id="tbl_lic_{{$emp->emp_id}}" name="hra" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                   
                                                                    <td><input type="text" value="{{ $emp->msc_tbl}}" id="tbl_msc_tbl_{{$emp->emp_id}}" name="govt_perks" style="width: 69px;font-size: 12px;text-align: center;"></td>
                                                                    <td><input type="text" value="{{ $emp->society + $emp->csf +$emp->lic + $emp->msc_tbl}}" name="govt_perks" style="width: 69px;font-size: 12px;text-align: center;"></td>   
                                                                    <td>
                                                                        <a href="javascript:void(0)" class="btn btn-info updateTblDeduct" data-empid="{{$emp->emp_id}}">Update</a>
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
        
                                                        </tbody>
        
        
                                                    </table>
                                                </div>
                                                <div class="tab-pane " id="empl_net_pay" role="tabpanel">
                                                    <table class="table table-bordered myTable " style="font-size: 11px"> 
                                                        <thead>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Employee Name</th>
                                                            <th class="text-center">Employee Id</th>
                                                            <th class="text-center">Gross Pay</th>
                                                            <th class="text-center">Total Deduction</th>
                                                            <th class="text-center">Table Deduction</th>
                                                            <th class="text-center">Pay To Bank</th>
                                                            {{-- <th class="text-center">Action</th> --}}
        
                                                        </thead>
        
                                                        <tbody class="text-center">
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($employee as $emp)
                                                                <tr>
                                                                    <td>{{$i}}</td>
                                                                    <td>{{$emp->name}}</td>
                                                                    <td>{{$emp->emp_id}}</td>
                                                                    <td>{{$gross=$emp->basic_pay + $emp->hra + $emp->ta_da + $emp->tpt + $emp->pers_pay +$emp->misc + $emp->govt_perks}}</td>
                                                                    <td>{{$ded=$emp->gpf_ref+$emp->cgeis+$emp->cghs+$emp->f_adv+$emp->rent_rec+$emp->tax_deduction+$emp->nps_deduction+$emp->misc}}</td>
                                                                    <td>{{$tbdd= $emp->society + $emp->csf +$emp->lic + $emp->msc_tbl}}</td>
                                                                    <td class="font-weight-bold">{{$gross - ($ded+$tbdd)}}</td>
                                                                    {{-- <td>
                                                                        <a href="javascript:void(0)" class="text-primary">View Salaray Slip</a>
                                                                    </td> --}}
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
        
                                                        </tbody>
        
        
                                                    </table>
                                                </div>
                                            </div>



                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="emp_sal_slip" role="tabpanel">
                                    <span class="text-danger">Note: Previous Month Generated Salary</span>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('salray-slip')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="salray-slip">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="w-100" border="1"  >
                                                                <tr>
                                                                    <th colspan="3"><u>STATION WORKSHOP EME, DELHI CANTT-10<br><br></u></th>
                                                                </tr>
                                                            
                                                                <tr>
                                                                    <td colspan="2">Payslip for the month : Dec 2022<br>Name: mohan sharma<br>
                                                                    DOB: 08/05/2022</td>
                                                                    <td>T.No : 209<br>Trade : H5,<br>DOA : 07/04/2022</td>
                                                                    <td rowspan="2">EOL(D)
                                                                        <hr style="border: 2px solid black;">EOL(D)
                                                                    </td>
                                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td style="text-align: center;">ENTITLEMENTS</td>
                                                                    <td style="text-align: center;">DEDUCTION</td>
                                                                    <td style="text-align: center;">TABLE DEDUCTION</td>
                                                                    
                                                                </tr>
                                                            
                                                                <tr>
                                                                    <td rowspan="3">
                                                                        BP : 404000<br>DA: 15424585<br>HRA : 0 <br>TPT : 49857<br>WA : 0<br>SPL PAY : 0
                                                                        <br>MISC : 0 
                                                                    </td>
                                                                    <td rowspan="3">
                                                                        AWL(D) : 0 <br> AWL(H) : 0<br>GPF : 12000<br>CGEIS : 30<br>CGHS : 250 <br>FA : 0<br>Rent Rec : 0<br>MISC : 0
                                                                    </td>
                                                                    <td rowspan="3">
                                                                        SOCIETY : 0<br>CWF : 0 <br>LIC : 12500<br>MISC : 0
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                   
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                    
                                                                <tr>
                                                              <td>TOTAL PAY : 60720 </td>
                                                              <td>TOTALDED:1254</td>
                                                              <td colspan="2">TOTAL TABLE GER: 1127</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">TOTAL PAYABLE AFTER DER : 548754</td>
                                                                    <td colspan="2">NETPAY : 48785</td>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td colspan="4">Remarks:<br><br></td>
                                                                </tr>
                                                    
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane " id="emp_pay_roll" role="tabpanel">
                                    <span class="text-danger">Note: Previous Month Generated Salary</span>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row" style="border-bottom: 1px solid black; padding-bottom:5px">
                                                <div class="col-md-1">
                                                    <label>Filter Type : </label>
                                                </div>
                                                <div class="col text-left">
                                                    <select name="filter_type">
                                                   
                                                        <option value="w">Weekly</option>
                                                        <option value="m" selected>Monthly</option>
                                                    </select>
                                                </div>
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('empMonthlyePaySlip')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                               
                                            </div>
                                            <div class="row mt-3" id="empMonthlyePaySlip">
                                                <style type="text/css" media="print">
                                                    /* body{margin: 10px 30px 10px 30px;padding:20px 20px 10px 20px;}    */
                                                    @page { size: landscape; }
                                                    
                                                  </style>
                                                <div class="col">
                                                    <div class="row" >
                                                        
                                                        <div class="col-md-12" id="">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <h5 style="text-align: center;"><u>STATION WORKSHOP EME, DELHI CANTT -10</u></h4>
                                                                    <h6><u>Pay Roll Of CIVILIANS (INDUSTRIAL) PERS</u></h6>
                                                                    <h6 style="text-align: end;">For the month : <u>{{date('M Y',strtotime("-1 months"))}}</u><br>
                                                                        No of working days <u>: {{$wd}} ({{date('M Y',strtotime("-1 months"))}})</u></h6>
                                                                        
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <table class="text-center w-100" border="1" style="font-size:11px">
                                                                        <tr>
                                                                            <th colspan="5"></th>
                                                                            {{-- <th colspan="3"></th> --}}
                                                                            <th colspan="9">Entitlements</th>
                                                                            <th colspan="12">Deductions</th>
                                                                        </tr>
                                                        
                                                                        <tr>
                                                                            <td rowspan="3">S No.</td>
                                                        
                                                                            <td colspan="5">No.Trade. Name</td>
                                                                            <th rowspan="3" >BP<br> Level</th>
                                                                            <th rowspan="3" >DA <br>34%</th>
                                                                            <th rowspan="3" >HRA <br> 27%</th>
                                                                            <th rowspan="3" >TPT</th>
                                                                            <th rowspan="3" >Pers <br> Pay</th>
                                                                            <th rowspan="3" >WA</th>
                                                                            <th rowspan="3" >Misc</th>
                                                                            <th rowspan="3" >Total Pay</th>
                                                                            <th rowspan="3" >EOL(H)</th>
                                                                            <th rowspan="3" >EOL(D)</th>
                                                                            {{-- <th rowspan="3" >GPF</th> --}}
                                                                            <th rowspan="3" >GPF<br>Ref</th>
                                                                            <th rowspan="3" >CGEIS</th>
                                                                            <th rowspan="3" >CGHS</th>
                                                                            <th rowspan="3" >F Adv</th>
                                                                            <th rowspan="3" >Rent <br> Rec</th>
                                                                            <th rowspan="3" >Misc<br>Ded</th>
                                                                            <th rowspan="3" >Tax<br> Ded</th>
                                                                            <th rowspan="3" >Total<br>Ded</th>
                                                                            <th rowspan="3" >Net Pay</th>
                                                                        </tr>
                                                        
                                                                        <tr>
                                                        
                                                                            <td>DOB</td>
                                                                            <td>GPF A/C</td>    
                                                                            <td>EOL(D)</td>
                                                                            <td>EOL(H)</td>
                                                                        </tr>
                                                        
                                                                        <tr>
                                                                            <td>DOA</td>
                                                                            <td>PAN NO</td>
                                                                            <td colspan="2">AADHAR NO</td>
                                                                        </tr>
                                                        
                                                                        <tr>
                                                                            <td> </td>
                                                                        </tr>
                                                        
                                                        
                                                        
                                                        
                                                        @php
                                                            $p=1;
                                                        @endphp
                                                                        @foreach ($salary as $empSal)
                                                                            <tr>
                                                                                <td rowspan="3">{{$p}}.</td>
                                                            
                                                                                <td colspan="5">T. No-{{$empSal->empdetails->t_no}} {{$empSal->empdetails->trade}} {{$empSal->empdetails->name}} (DOR {{date('d/m/Y',strtotime($empSal->empdetails->do_retirement))}})</td>
                                                                                <td rowspan="2" >{{$empSal->basic_pay}}</td>
                                                                                <td rowspan="2" >{{$empSal->da}}</td>
                                                                                <td rowspan="2" >{{$empSal->hra}} </td>
                                                                                <td rowspan="2" >{{$empSal->tpt}}</td>
                                                                                <td rowspan="2" >{{$empSal->pers_pay}}</td>
                                                                                <td rowspan="2" >{{$empSal->wa}}</td>
                                                                                <td rowspan="2" >{{$empSal->misc}}</td>
                                                                                <td rowspan="2" >
                                                                                    {{$totalPay=$empSal->total_pay}}
                                                                                </td>
                                                                                <td rowspan="2" >{{$empSal->eol_h}}</td>
                                                                                <td rowspan="2" >{{$empSal->eol_d}}</td>
                                                                                <td rowspan="2" >{{$empSal->gpf_ref}}</td>
                                                                                <td rowspan="2" >{{$empSal->cgeis}}</td>
                                                                                <td rowspan="2" >{{$empSal->cghs}}</td>
                                                                                <td rowspan="2" >{{$empSal->f_adv}}</td>
                                                                                <td rowspan="2" >{{$empSal->rent_rec}}</td>
                                                                                <td rowspan="2" >{{$empSal->misc_ded}}</td>
                                                                                <td rowspan="2" >0</td>
                                                                                <td rowspan="2" >
                                                                                    {{$totalDed=$empSal->total_deduction}}
                                                                                </td>
                                                                                <td rowspan="2" >
                                                                                    @if ($totalPay>0)
                                                                                        {{$netPay=$totalPay-$totalDed}}
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                        
                                                                                <td>{{date('d/m/Y',strtotime($empSal->empdetails->dob))}}</td>
                                                                                <td>{{$empSal->empdetails->gpf_no}}</td>
                                                                                <td>0</td>
                                                                                <td>0</td>
                                                                            </tr>
                                                            
                                                                            <tr>
                                                                                <td>{{date('d/m/Y',strtotime($empSal->empdetails->doa))}}</td>
                                                                                <td>{{$empSal->empdetails->pan_no}}</td>
                                                                                <td colspan="2">{{$empSal->empdetails->aadhar_no}}</td>
                                                                                <th colspan="22">
                                                                                    @if ($empSal->tax_ded==0)
                                                                                        Remarks: IT @ {{$empSal->tax_ded}}/- ({{$p}})
                                                                                    @endif
                                                                                    
                                                                                </th>
                                                                            </tr>
                                                                            @php
                                                                                $p++
                                                                            @endphp
                                                                        @endforeach
                                                        
                                                        
                                                                       
                                                        
                                                                        
                                                                        <tr>
                                                                            <td colspan="25"> Page Total 1</td>
                                                                        </tr>
                                                        
                                                        
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="pagebreak"> </div>
                                                    
                                                </div>



                                                
                                                
                                                
                                                
                                                


                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="tab-pane  "  id="emp_pay_roll_days" role="tabpanel">
                                    <span class="text-danger">Note: Previous Month Generated Salary</span>
                                    @php
                                        $totalDays=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime("-1 months")),date('Y',strtotime("-1 months")));
                                        // $number = cal_days_in_month(CAL_GREGORIAN, 8, 2003); // 31
                                        // echo "There were ".$number ."days in August 2003";
                                    @endphp                             
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('dayWisePayRole')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="dayWisePayRole">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>
                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            <h3 style="text-align: center;" class="mt-5"><u>STATION WORKSHOP EME, DELHI CANTT - 10 </u></h3>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <h6> OF INDUSTRIAL CIVILIANS FOR THE MONTH OF {{date('M',strtotime('-1 months')).' '.date('Y',strtotime('-1 months'))}}</h6>
                                                        </div>
                                                        <div class="col">
                                                            <p style="text-align: end;">Working Days = {{$wd}}
                                                                
                                                                <br>
                                                                {{date('M',strtotime('-1 months')).' '.date('Y',strtotime('-1 months'))}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <table class="w-100" border="1" style="font-size: 12px">
                                                                
        
                                                                <tr>
                                                                    <th>Trade & Name</th>
                                                                    @for($i = 1; $i <= $totalDays; $i++)
                                                                        <th class="text-center tdd">{{$i}}</th>
                                                                    @endfor
                                                                    
                                                                    <th class="text-center ">DO II No.</th>
                                                                </tr>
            
            
                                                                @php
                                                                     $d=1;
                                                                 @endphp
                                                                    @foreach ($attenArray as $empSal)
                                                                        <tr>
                                                                            <td>({{$empSal['trade']}}) {{$empSal['name']}}</td>
                                                                            @php
                                                                                $weekend=false;
                                                                            @endphp
                                                                            @for ($i = 0; $i < $totalDays; $i++)
                                                                                @php

                                                                                    $da=$i+1;
                                                                                  
                                                                                    $year=date('Y',strtotime('-1 months'));
                                                                                    $month=date('m',strtotime('-1 months'));
                                                                                    $day=$da;
                                                                                     $startdate = strtotime($year . '-' . $month .'-'.$day);
                                                                                    // echo ' | '.date('D',$startdate) .' | ';
                                                                                //    echo      $startdate = strtotime( . '-' .  . '-01'));
                                                                                //    var_dump($startdate);
                                                                                   // $d_a_te=date($da.' m y',strtotime('-1months'));
                                                                                    // $d_a_te=strtotime($d_a_te);
                                                                                    // echo date('D',strtotime($d_a_te));
                                                                                    // echo $currentDate=startdate;
                                                                                    // echo ' || '.date('D',$currentDate); 
                                                                                    if(date('D',$startdate)=='Sun'){
                                                                                        $weekend=true;
                                                                                        // echo 'TRUE';
                                                                                    }else{
                                                                                        $weekend=false;
                                                                                    }
                                                                                //    echo date('D',strtotime());
                                                                                @endphp 
                                                                                @if (isset($empSal['leaves']) && count($empSal['leaves'])>0)
                                                                                    @php
                                                                                        $onLeave=false;
                                                                                        $ct="";
                                                                                    @endphp
                                                                                    @foreach ($empSal['leaves'] as $item)
                                                                                        @php
                                                                                       
                                                                                            $from=date('d',strtotime($item['from']));
                                                                                            $to=date('d',strtotime($item['to']));
                                                                                            for($j=$from; $j<=$to; $j++){
                                                                                                if($j==$da){
                                                                                                    $onLeave=true;
                                                                                                    $ct=$item['category'];
                                                                                                }
                                                                                            }
                                                                                            
                                                                                        @endphp
                                                                                    
                                                                                    @endforeach
                                                                                    
                                                                                @else
                                                                                    @php
                                                                                        $onLeave=false;
                                                                                        // $attenSt='A';
                                                                                    @endphp
                                                                                @endif

                                                                                @if (isset($empSal['attendance']) && count($empSal['attendance'])>0)
                                                                                    @php
                                                                                        $presentTrue=false;
                                                                                    @endphp
                                                                                    @foreach ($empSal['attendance'] as $item)
                                                                                    @php
                                                                                        $day=date('d',strtotime($item['attendance_date']));
                                                                                        if($day==$da){
                                                                                            $presentTrue=true;
                                                                                            
                                                                                        }
                                                                                    @endphp
                                                                                      
                                                                                    @endforeach
                                                                                    
                                                                                @else
                                                                                    @php
                                                                                        $presentTrue=false;
                                                                                        // $attenSt='A';
                                                                                    @endphp
                                                                                @endif
                                                                               
                                                                                @php
                                                                                if($weekend){
                                                                                    $black='bg-dark';
                                                                                    $attenSt='';
                                                                                    $class='';
                                                                                }else{
                                                                                    $black='';
                                                                                    if($onLeave){
                                                                                        $attenSt=$ct;
                                                                                        $class="text-success font-weight-bold";
                                                                                        
                                                                                    }else{
                                                                                       
                                                                                        if($presentTrue){
                                                                                            $attenSt='P';
                                                                                            $class="text-primary font-weight-bold";
                                                                                        }else{
                                                                                            $attenSt='A';
                                                                                            $class="text-danger";
                                                                                        }
                                                                                    }
                                                                                    
                                                                                }
                                                                                    
                                                                                @endphp
                                                                                

                                                                                <td class="tdd text-center {{$class}} {{$black}}">{{$attenSt}}</td>
                                                                                
                                                                            @endfor
                                                                            <td class=" text-center">-do- 1254255254</td>
                                                                        </tr>
                                                                    @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>  
                                    </div>
                                </div>
                                <div class="tab-pane " id="comm_pay_roll" role="tabpanel">
                                    
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('commWisePayRole')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="commWisePayRole">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            <h4 style="text-align: center;" class="mt-5"><u>STATION WORKSHOP EME, DELHI CANTT - 10</u></h4>

                                                            <h6 style="text-align: center;"><u>SUMMARY OF PAY ROLL CIVILIANS (INDUSTRIAL PERS)</u></h6>

                                                            <P style="text-align: end;"> For the month: <u>Sep 2022</u></P>

                                                        </div>
                                                    </div>
                                                    <div class="row mt-1">
                                                        <div class="col">
                                                            <table class="w-100" border="1" style="font-size:11px">
                                                                <tr>
                                                                    <th>No of Page </th>
                                                                    <th>Basic Pay</th>
                                                                    <th>DA</th>
                                                                    <th>HRA</th>
                                                                    <th>TPT</th>
                                                                    <th>Pers</th>
                                                                    <th>WA</th>
                                                                    <th>Misc</th>
                                                                    <th>Total Pay</th>
                                                                    <th>EOL</th>
                                                                    <th>EOL</th>
                                                                    <th>GPF</th>
                                                                    <th>CGEIS</th>
                                                                    <th>CGHS</th>
                                                                    <th>F Adv</th>
                                                                    <th>Rent </th>
                                                                    <th>Misc</th>
                                                                    <th>Tax</th>
                                                                    <th>Total</th>
                                                                    <th>Net Pay</th>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>Pay</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>Ref</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>Rec</td>
                                                                    <td>Ded</td>
                                                                    <td>Ded</td>
                                                                    <td>Ded</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>


                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>1250145</td>
                                                                    <td>895746</td>
                                                                    <td>124582</td>
                                                                    <td>24569</td>
                                                                    <td>25987</td>
                                                                    <td>2548</td>
                                                                    <td>2548</td>
                                                                    <td>5897746</td>
                                                                    <td>2315046</td>
                                                                    <td>529874</td>
                                                                    <td>54896</td>
                                                                    <td>214587</td>
                                                                    <td>89574</td>
                                                                    <td>58974</td>
                                                                    <td>4875</td>
                                                                    <td>78456</td>
                                                                    <td>25461</td>
                                                                    <td>58749</td>
                                                                </tr>


                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>1250145</td>
                                                                    <td>895746</td>
                                                                    <td>124582</td>
                                                                    <td>24569</td>
                                                                    <td>25987</td>
                                                                    <td>2548</td>
                                                                    <td>2548</td>
                                                                    <td>5897746</td>
                                                                    <td>2315046</td>
                                                                    <td>529874</td>
                                                                    <td>54896</td>
                                                                    <td>214587</td>
                                                                    <td>89574</td>
                                                                    <td>58974</td>
                                                                    <td>4875</td>
                                                                    <td>78456</td>
                                                                    <td>25461</td>
                                                                    <td>874956</td>
                                                                    <td>58479</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>1250145</td>
                                                                    <td>895746</td>
                                                                    <td>124582</td>
                                                                    <td>24569</td>
                                                                    <td>25987</td>
                                                                    <td>2548</td>
                                                                    <td>2548</td>
                                                                    <td>5897746</td>
                                                                    <td>2315046</td>
                                                                    <td>529874</td>
                                                                    <td>54896</td>
                                                                    <td>214587</td>
                                                                    <td>89574</td>
                                                                    <td>58974</td>
                                                                    <td>4875</td>
                                                                    <td>78456</td>
                                                                    <td>25461</td>
                                                                    <td>58794</td>
                                                                    <td>859746</td>
                                                                </tr>


                                                                <tr>
                                                                    <td>Grand Total</td>
                                                                    <td>1250145</td>
                                                                    <td>895746</td>
                                                                    <td>124582</td>
                                                                    <td>24569</td>
                                                                    <td>25987</td>
                                                                    <td>2548</td>
                                                                    <td>2548</td>
                                                                    <td>5897746</td>
                                                                    <td>2315046</td>
                                                                    <td>529874</td>
                                                                    <td>54896</td>
                                                                    <td>214587</td>
                                                                    <td>89574</td>
                                                                    <td>58974</td>
                                                                    <td>4875</td>
                                                                    <td>78456</td>
                                                                    <td>25461</td>
                                                                    <td>5482</td>
                                                                    <td>2549</td>
                                                                </tr>

                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3" style="font-size:11px">
                                                        <div class="col text-left">
                                                            Tele : 36584/25694712<br>
                                                            50601/Civ/PR/IP<br>
                                                            LAO<br>505 Army Base Wksp<br>Delhi Cantt-10
                                                        </div>
                                                        <div class="col text-right">
                                                            Station Wksp EME<br> Delhi Cantt-10<br>Oct 2022
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="comm_pay_roll_nps" role="tabpanel">
                                    <span class="text-danger">Note: Previous Month Generated Salary</span>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('commWisePayRoleNps')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="commWisePayRoleNps">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            <h5 style="text-align: center;" class="mt-2"><u>STATION WORKSHOP EME, DELHI CANTT - 10</u></h5>

                                                            <h6><u>SUMMARY OF PAY ROLL CIVILIANS(INDUSTRIAL RERS - NPS)</u></h6>

                                                            <p style="text-align: end;"> For the month <u>Sep 2022</u></p>
                                                        </div>   
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="w-100" border="1" style="font-size:11px">


                                                                <tr>
                                                                    <th>Page No.</th>
                                                                    <th>BP</th>
                                                                    <th>DA</th>
                                                                    <th>HRA</th>
                                                                    <th>TPT</th>
                                                                    <th>Pres pay</th>
                                                                    <th>Govt Contri</th>
                                                                    <th>WA</th>
                                                                    <th>Misc</th>
                                                                    <th>Total Pay</th>
                                                                    <th> EOL(D)</th>
                                                                    <th>EOL(H)</th>
                                                                    <th>GMC Contri NPS</th>
                                                                    <th>Indl Con<br>Tier(NPS)</th>
                                                                    <th>CGEIS </th>
                                                                    <th>CGHS</th>
                                                                    <th>Rent Rec</th>
                                                                    <th>Misc Ded</th>
                                                                    <th>Tax Ded</th>
                                                                    <th>Total ded</th>
                                                                    <th>Net Pay</th>
    
    
                                                                </tr>
    
                                                                @php
                                                                     $d=1;
                                                                     $bp=0;
                                                                     $da=0;
                                                                     $hra=0;
                                                                     $tpt=0;
                                                                     $pp=0;
                                                                     $wa=0;
                                                                     $ms=0;
                                                                     $ttl=0;
                                                                     $cghs=0;
                                                                     $cgeis=0;
                                                                     $eold=0;
                                                                     $eolh=0;
                                                                 @endphp
                                                                    @foreach ($salary as $empSal)
                                                                        <tr>
                                                                            <td>{{$d++}}</td>
                                                                            <td>{{$empSal->basic_pay}}</td>
                                                                            <td>{{$empSal->da}}</td>
                                                                            <td>{{$empSal->hra}}</td>
                                                                            <td>{{$empSal->tpt}}</td>
                                                                            <td>{{$empSal->pers_pay}}</td>
                                                                            <td>{{$empSal->wa}}</td>
                                                                            <td>{{$empSal->misc}}</td>
                                                                            <td>0</td>
                                                                            <td> {{$totalPay=$empSal->basic_pay + $empSal->ta_da + $empSal->hra + $empSal->tpt + $empSal->pers_pay}}</td>
                                                                            <td>{{$empSal->eol_d}}</td>
                                                                            <td>{{$empSal->eol_h}}</td>
                                                                            <td>48851</td>
                                                                            <td>54884</td>
                                                                            {{-- <td>154848 </td> --}}
                                                                            <td>{{$empSal->cgeis}}</td>
                                                                            <td>{{$empSal->cghs}}</td>
                                                                            
                                                                            <td>{{$empSal->rent_rec}}</td>
                                                                            <td>{{$empSal->misc_ded}}</td>
                                                                            <td>0</td>
                                                                            <td>{{$totalDed=$empSal->gpf_ref + $empSal->cgeis + $empSal->cghs + $empSal->f_adv + $empSal->rent_rec+$empSal->rent_rec +$empSal->misc_ded}}</td>
                                                                            <td>@if ($totalPay>0)
                                                                                {{$netPay=$totalPay-$totalDed}}
                                                                            @endif</td>
            
            
                                                                        </tr>
                                                                        @php
                                                                            $bp+=$empSal->basic_pay;
                                                                            $da+=$empSal->da;
                                                                            $hra+=$empSal->hra;
                                                                            $tpt+=$empSal->tpt;
                                                                            $pp+=$empSal->pers_pay;
                                                                            $ms+=$empSal->misc;
                                                                            $wa+=$empSal->wa;
                                                                            $ttl+=$totalPay=$empSal->basic_pay + $empSal->ta_da + $empSal->hra + $empSal->tpt + $empSal->pers_pay;
                                                                            $eolh+=$empSal->eol_h;
                                                                            $eold+=$empSal->eol_d;
                                                                            $cghs+=$empSal->cghs;
                                                                            $cgeis+=$empSal->cgeis;
                                                                        @endphp

                                                                        

                                                                    @endforeach

                                                                <tr>
                                                                    <th>Grand Total Rs.</th>
                                                                    <th>{{$bp}}</th>
                                                                    <th>{{$da}}</th>
                                                                    <th>{{$hra}}</th>
                                                                    <th>{{$tpt}}</th>
                                                                    <th>{{$pp}}</th>
                                                                    <th>0</th>
                                                                    <th>{{$wa}}</th>
                                                                    <th>{{$ms}}</th>
                                                                    <th>{{$ttl}}</th>
                                                                    <th>{{$eold}}</th>
                                                                    <th>{{$eolh}}</th>
                                                                    <th>15</th>
                                                                    <th>54</th>
                                                                    <th>{{$cgeis}}</th>
                                                                    <th>{{$cghs}}</th>
                                                                    <th colspan="5"></th>
                                                                </tr>
    
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="arrear_da" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('arrear_da_div')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="arrear_da_div">
                                                <style type="text/css" media="print">
                                                    /* body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}    */
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            <h4 style="text-align: center;" class="mt-5">STATION WORKSHOP EME, DELHI CANTT-110010 </h4>
                                                            <h6>SY CHECK ROLL NO 50601/CIV/IP DT <span class="mx-5">OCT 2022 ARREAR OF FOR THE RERIOD OF JUL 2022 TOP SEP
                                                                    2022</span></h6>
                                                            <P>Declared increase in DA=</P>

                                                        </div>   
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="w-100" border="1">
                                                                <tr>
                                                                    <th>Ser.</th>
                                                                    <th>T.NO. TRADE NAME</th>
                                                                    <th>Month</th>
                                                                    <th>Basic pay</th>
                                                                    <th>Month Days</th>
                                                                    <th>Arr of DA 4%</th>
                                                                    <th>TA</th>
                                                                    <th>Arr of TA 4%</th>
                                                                    <th></th>
                                                                    <th>Total (f+h)</th>
                                                                    <th>EOL Days</th>
                                                                    <th>EOL Rec</th>
                                                                    <th>Total Rec</th>
                                                                    <th>Net Amt Payable</th>
                                                                    <th>Sig4744</th>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td>(a)</td>
                                                                    <td>(b)</td>
                                                                    <td>(c)</td>
                                                                    <td>(d)</td>
                                                                    <td>(e)</td>
                                                                    <td>(f)</td>
                                                                    <td>(g)</td>
                                                                    <td>(h)</td>
                                                                    <td>()</td>
                                                                    <td>(j)</td>
                                                                    <td>(k)</td>
                                                                    <td>(i)</td>
                                                                    <td>(m)</td>
                                                                    <td>(n)</td>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td rowspan="3">1.</td>
                                                                    <td rowspan="3">T.No 34 VM MV (MCM) Sh Rajinder kumar</td>
                                                                    <td>Jul</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                    
                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td>Jul</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td></td>
                                                    
                                                    
                                                                </tr>
                                                    
                                                    
                                                                <tr>
                                                                    <td>Jul</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td></td>
                                                    
                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>0</td>
                                                                </tr>
                                                    
                                                    
                                                    
                                                                <tr>
                                                                    <td rowspan="3">1.</td>
                                                                    <td rowspan="3">T.No 34 VM MV (MCM) Sh Rajinder kumar</td>
                                                                    <td>Jul</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                    
                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td>Jul</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td></td>
                                                    
                                                    
                                                                </tr>
                                                    
                                                    
                                                                <tr>
                                                                    <td>Jul</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td>20120</td>
                                                                    <td>2541</td>
                                                                    <td></td>
                                                    
                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>0</td>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                    <td><b> Total</b></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>2548</td>
                                                                    <td></td>
                                                                    <td>1524</td>
                                                                    <td></td>
                                                                    <td>2456</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>5684</td>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                    
                                                    
                                                            </table>
                                                            <h6 style="text-align: center;" class="mt-5">SUMMARY FROM PAGE NO 1 TO 5</h6>


                                                            <table class="w-100" border="1" style="font-size:11px">
                                                                <tr>
                                                                    <th>Page No </th>
                                                                    <th>DA 4%</th>
                                                                    <th></th>
                                                                    <th>TA 4%</th>
                                                                    <th></th>
                                                                    <th>Total(DA+TA)</th>
                                                                    <th>EOL REC</th>
                                                                    <th>Net Amt</th>
                                                                </tr>

                                                                <tr>
                                                                    <td>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1</td>
                                                                    <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                    <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                    <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                    <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                    <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                    <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                    <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                </tr>


                                                                <tr>
                                                                    <th>GRAND TOTAL RS</th>
                                                                    <th>124589</th>
                                                                    <th></th>
                                                                    <th>10458</th>
                                                                    <th></th>
                                                                    <th>02582</th>
                                                                    <th>0</th>
                                                                    <th>1245877</th>
                                                                </tr>

                                                            </table>
                                                            <p style="text-align: center;"> (Rupees one lakh forty Thousand one Hundred seventy Six Only)</p>

                                                            <p style="text-align: end;">Asst Exce<br>Civ Est Office<br>Stn Wksp EME Delhi Cantt-10</p>


                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="arrear_da_nps" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('arrea_da_nps_div')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="arrea_da_nps_div">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            

                                                            <h5 style="text-align: center;"><u>STATION WORKSHOP EME, DELHI CANTT - 110010</u></h5>
                                                            <H6 style="text-align: center;"><u>SY CHECK ROLL NO 50601/CIV/NPS/ DT OCT 2022: ARREAR OF DA FOR THE PERIOD JUL
                                                                    2022 TO SEP 2022</u></H6>
                                                        </div>   
                                                    </div>
                                                    <div class="row my-2">
                                                        <div class="col-md-12">
                                                            <table class="w-100" border="1" style="font-size: 11px">
                                                                <tr>
                                                                    <th>Ser</th>
                                                                    <th>T.No Trade & Name</th>
                                                                    <th>Month</th>
                                                                    <th>Basic Pay </th>
                                                                    <th>Month Days</th>
                                                                    <th>DA</th>
                                                                    <th>TA</th>
                                                                    <th>Arr of TA</th>
                                                                    <th>Arr of HRA</th>
                                                                    <th>Total (F+H+L)</th>
                                                                    <th>EOL Days</th>
                                                                    <th>EOL Rec</th>
                                                                    <th>Total (-i)</th>
                                                                    <th>GHC 14 % of DA</th>
                                                                    <th>Total Arrear Of DA</th>
                                                                    <th>GMC Rec</th>
                                                                    <th>CPS Rec</th>
                                                                    <th>Total Rec</th>
                                                                    <th>Amt payable(O-r)</th>
                                                                    <th>Signatu</th>
                                                                </tr>
                                                    
                                                    
                                                                <tr>
                                                                    <td>(a)</td>
                                                                    <td>(b)</td>
                                                                    <td>(c)</td>
                                                                    <td>(d)</td>
                                                                    <td>(e)</td>
                                                                    <td>(f)</td>
                                                                    <td>(g)</td>
                                                                    <td>(h)</td>
                                                                    <td>(i)</td>
                                                                    <td>(j)</td>
                                                                    <td>(k)</td>
                                                                    <td>(l)</td>
                                                                    <td>(m)</td>
                                                                    <td>(n)</td>
                                                                    <td>(o)</td>
                                                                    <td>(p)</td>
                                                                    <td>(q)</td>
                                                                    <td>(r)</td>
                                                                    <td>(s)</td>
                                                                    <td>(t)</td>
                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td>1.</td>
                                                                    <td>T.No 402 TCM Sh Dayal Singh</td>
                                                                    <td>Jul<br>Aug<br>Sep</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                    
                                                    
                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>24589</td>
                                                                    <td>48795</td>
                                                                    <td>7854</td>
                                                                    <td>9857</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                                <tr>
                                                                    <td>2.</td>
                                                                    <td>T.No 402 TCM Sh Dayal Singh</td>
                                                                    <td>Jul<br>Aug<br>Sep</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>54854<br>89455<br>24587</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                    
                                                    
                                                    
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>24589</td>
                                                                    <td>48795</td>
                                                                    <td>7854</td>
                                                                    <td>9857</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                    
                                                    
                                                    
                                                                <tr>
                                                                    <td></td>
                                                                    <td><b>TOTAL RS</b></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>489</td>
                                                                    <td>4789</td>
                                                                    <td>24589</td>
                                                                    <td>48795</td>
                                                                    <td>7854</td>
                                                                    <td>9857</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td>7895</td>
                                                                    <td>487</td>
                                                                    <td>558745</td>
                                                                    <td>4579</td>
                                                                    <td></td>
                                                                </tr>
                                                    
                                                    
                                                    
                                                    
                                                            </table>
                                                    
                                                    
                                                            <table class="mt-1 w-100" border="1" style="font-size: 11px">
                                                                <tr>
                                                                    <th>PAGE NO</th>
                                                                    <th>TOTAL INCL GMC</th>
                                                                    <th colspan="3">NPS</th>
                                                                    <th>TOTAL</th>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td>PAGE 1<br>PAGE 2 <br> PAGE 3<br>PAGE 4 <br> PAGE 5<br>PAGE 6</td>
                                                                    <td>245844<br>54869<br>15897<br>24589<br>56987<br>589746<br>5428</td>
                                                                    <td>00000</td>
                                                                    <td>25489<br>58744<br>568974<br>897456<br>897456<br>58976<br>457896</td>
                                                                    <td>00000</td>
                                                                    <td>589745<br>45896<br>578946<br>589466<br>45894<br>58496</td>
                                                                </tr>
                                                    
                                                                <tr>
                                                                    <td><b>TOTAL RS</b></td>
                                                                    <td>2548975</td>
                                                                    <td colspan="3">3598745</td>
                                                                    <td>57894855</td>
                                                                </tr>
                                                            </table>
                                                    
                                                            <h6 style="text-align: center;">(Rupees one Lakh forty one Thousand Two Hundred Eighty one only)</h6>
                                                    
                                                    
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="bill_pay" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('pay_bill_div')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="pay_bill_div">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            

                                                            <h5 style="text-align: center;" class="mt-5"><u>STATION WORKSHOP EME, DELHI CANTT-110010</u></h5>
                                                            <h6 style="text-align: center;"><u>CHECK ROLL (PAY BILL) IN R/O APPRENTICESHIP CANDIDATE THE MONTH OF OCT
                                                                    2022</u></h6>
                                                        </div>   
                                                    </div>
                                                    <div class="row my-2">
                                                        <div class="col-md-12">
                                                            
                                                                <table class="w-100" border="1" style="font-size: 11px">


                                                                    <tr>
                                                                        <th>Ser No.</th>
                                                                        <th>Reg No.</th>
                                                                        <th>Name</th>
                                                                        <th>Father's Name</th>
                                                                        <th>DOB</th>
                                                                        <th>Trade</th>
                                                                        <th>DOA</th>
                                                                        <th>P Days</th>
                                                                        <th>Days in Month</th>
                                                                        <th>Stipend in Rs</th>
                                                                        <th>Net Payab</th>
                                                                        <th>Remarks</th>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>1.</td>
                                                                        <td>A0921480337</td>
                                                                        <td>mohan.vishwkarma</td>
                                                                        <td>RAJENDER.PARSAD.VISHWKARMA</td>
                                                                        <td>25.07.2003</td>
                                                                        <td>Mechanic.Auto.Body.Painting</td>
                                                                        <td>09.May.22</td>
                                                                        <td>24</td>
                                                                        <td>31</td>
                                                                        <td>1200</td>
                                                                        <td>1200</td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr>
                                                                        <td>2.</td>
                                                                        <td>A0921480337</td>
                                                                        <td>mohan.vishwkarma</td>
                                                                        <td>RAJENDER.PARSAD.VISHWKARMA</td>
                                                                        <td>25.07.2003</td>
                                                                        <td>Mechanic.Auto.Body.Painting</td>
                                                                        <td>09.May.22</td>
                                                                        <td>24</td>
                                                                        <td>31</td>
                                                                        <td>1200</td>
                                                                        <td>1200</td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr>
                                                                        <td>3.</td>
                                                                        <td>A0921480337</td>
                                                                        <td>mohan.vishwkarma</td>
                                                                        <td>RAJENDER.PARSAD.VISHWKARMA</td>
                                                                        <td>25.07.2003</td>
                                                                        <td>Mechanic.Auto.Body.Painting</td>
                                                                        <td>09.May.22</td>
                                                                        <td>24</td>
                                                                        <td>31</td>
                                                                        <td>1200</td>
                                                                        <td>1200</td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr>
                                                                        <td>4.</td>
                                                                        <td>A0921480337</td>
                                                                        <td>mohan.vishwkarma</td>
                                                                        <td>RAJENDER.PARSAD.VISHWKARMA</td>
                                                                        <td>25.07.2003</td>
                                                                        <td>Mechanic.Auto.Body.Painting</td>
                                                                        <td>09.May.22</td>
                                                                        <td>24</td>
                                                                        <td>31</td>
                                                                        <td>1200</td>
                                                                        <td>1200</td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="5"></td>
                                                                        <td colspan="5">Total Rs</td>
                                                                        <td>98574</td>
                                                                        <td></td>
                                                                    </tr>

                                                                </table>


                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
