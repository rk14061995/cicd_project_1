@extends('employee.layouts.main')
@section('main')
    <!-- end topbar -->
    <!-- dashboard inner -->
    <div class="midde_cont mt-2">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="col text-left">
                            
                            <label class="font-weight-bold">Unit Bills & Arrears</label>
                               
                        </div>
                       
                    </div>
                </div>
            </div>
            
            
            <div class="row my-1">
                {{-- <style>
                    .tdd{
                        width:20px;
                        height: 20px;
                    }
                    input {
                        border-top-style: hidden;
                        border-right-style: hidden;
                        border-left-style: hidden;
                        border-bottom-style: groove;
                        /* background-color: #eee; */
                        }

                        .no-outline:focus {
                        outline: none;
                        }
                </style> --}}
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" role="tablist">
                               
                                <li class="nav-item ">
                                    <a class="nav-link active" data-toggle="tab" href="#arrear_da" role="tab">Arrear DA</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" data-toggle="tab" href="#arrear_da_nps" role="tab">Arrear DA(NPS) </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" data-toggle="tab" href="#bill_pay" role="tab">Bill Pay </a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" data-toggle="tab" href="#emp_sal_slip" role="tab">Salary Slip </a>
                                </li>
                                


                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                               
                                <div class="tab-pane active" id="arrear_da" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">

                                            <ul class="nav nav-tabs" role="tablist">
                               
                                                <li class="nav-item ">
                                                    <a class="nav-link " data-toggle="tab" href="#arrear_da_crt" role="tab">Arrear DA</a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link active" data-toggle="tab" href="#arrear_da_list" role="tab">Arrear List</a>
                                                </li>
                                                {{-- <li class="nav-item ">
                                                    <a class="nav-link" data-toggle="tab" href="#bill_pay" role="tab">Bill Pay </a>
                                                </li>
                                                <li class="nav-item d-none">
                                                    <a class="nav-link" data-toggle="tab" href="#emp_sal_slip" role="tab">Salary Slip </a>
                                                </li> --}}
                                                
                
                
                                            </ul><!-- Tab panes -->
                                            <div class="tab-content">
                               
                                                <div class="tab-pane " id="arrear_da_crt" role="tabpanel">
                                                    <form>
                                                        <div class="row">
                                                            <div class="col">
                                                                <label>Employee</label>
                                                                <select class="form-control">
                                                                    <option value="0">Select</option>
                                                                    <option value="0">Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label>Month</label>
                                                                <select class="form-control">
                                                                    <option value="0">Select</option>
                                                                    @for ($i = 1; $i <= 12; $i++)
                                                                        <option value="{{$i}}">{{$i}}</option>
                                                                    @endfor
                                                                    
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label>Basic Pay</label>
                                                                <select class="form-control">
                                                                    <option value="0">Select</option>
                                                                    <option value="0">Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label>Month Days</label>
                                                                <select class="form-control">
                                                                    <option value="0">Select</option>
                                                                    <option value="0">Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label>Arr of DA 4% </label>
                                                                <select class="form-control">
                                                                    <option value="0">Select</option>
                                                                    <option value="0">Select</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane active" id="arrear_da_list" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col text-right">
                                                            <a href="javascript:printPdf('arrear_da_div')" class="text-primary"><i
                                                                    class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="arrear_da_div">
                                                        <style type="text/css" media="print">
                                                            /* body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}    */
                                                            @page { size: landscape; }
                                                            
                                                          </style>
        
        
                                                        <div class="col">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <h4 style="text-align: center;" class="mt-5">STATION WORKSHOP EME, DELHI CANTT-110010 </h4>
                                                                    <h6>SY CHECK ROLL NO 50601/CIV/IP DT <span class="mx-5">OCT 2022 ARREAR OF FOR THE RERIOD OF JUL 2022 TOP SEP
                                                                            2022</span></h6>
                                                                    <P>Declared increase in DA=</P>
        
                                                                </div>   
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="w-100" border="1">
                                                                        <tr>
                                                                            <th>Ser.</th>
                                                                            <th>T.NO. TRADE NAME</th>
                                                                            <th>Month</th>
                                                                            <th>Basic pay</th>
                                                                            <th>Month Days</th>
                                                                            <th>Arr of DA 4%</th>
                                                                            <th>TA</th>
                                                                            <th>Arr of TA 4%</th>
                                                                            <th></th>
                                                                            <th>Total (f+h)</th>
                                                                            <th>EOL Days</th>
                                                                            <th>EOL Rec</th>
                                                                            <th>Total Rec</th>
                                                                            <th>Net Amt Payable</th>
                                                                            <th>Sig4744</th>
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td>(a)</td>
                                                                            <td>(b)</td>
                                                                            <td>(c)</td>
                                                                            <td>(d)</td>
                                                                            <td>(e)</td>
                                                                            <td>(f)</td>
                                                                            <td>(g)</td>
                                                                            <td>(h)</td>
                                                                            <td>()</td>
                                                                            <td>(j)</td>
                                                                            <td>(k)</td>
                                                                            <td>(i)</td>
                                                                            <td>(m)</td>
                                                                            <td>(n)</td>
                                                                            <td></td>
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td rowspan="3">1.</td>
                                                                            <td rowspan="3">T.No 34 VM MV (MCM) Sh Rajinder kumar</td>
                                                                            <td>Jul</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                            
                                                            
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td>Jul</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td></td>
                                                            
                                                            
                                                                        </tr>
                                                            
                                                            
                                                                        <tr>
                                                                            <td>Jul</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td></td>
                                                            
                                                            
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>0</td>
                                                                        </tr>
                                                            
                                                            
                                                            
                                                                        <tr>
                                                                            <td rowspan="3">1.</td>
                                                                            <td rowspan="3">T.No 34 VM MV (MCM) Sh Rajinder kumar</td>
                                                                            <td>Jul</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                            
                                                            
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td>Jul</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td></td>
                                                            
                                                            
                                                                        </tr>
                                                            
                                                            
                                                                        <tr>
                                                                            <td>Jul</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td>20120</td>
                                                                            <td>2541</td>
                                                                            <td></td>
                                                            
                                                            
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>0</td>
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td></td>
                                                                            <td><b> Total</b></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>2548</td>
                                                                            <td></td>
                                                                            <td>1524</td>
                                                                            <td></td>
                                                                            <td>2456</td>
                                                                            <td>0</td>
                                                                            <td>0</td>
                                                                            <td>0</td>
                                                                            <td>5684</td>
                                                                            <td></td>
                                                                        </tr>
                                                            
                                                            
                                                            
                                                                    </table>
                                                                    <h6 style="text-align: center;" class="mt-5">SUMMARY FROM PAGE NO 1 TO 5</h6>
        
        
                                                                    <table class="w-100" border="1" style="font-size:11px">
                                                                        <tr>
                                                                            <th>Page No </th>
                                                                            <th>DA 4%</th>
                                                                            <th></th>
                                                                            <th>TA 4%</th>
                                                                            <th></th>
                                                                            <th>Total(DA+TA)</th>
                                                                            <th>EOL REC</th>
                                                                            <th>Net Amt</th>
                                                                        </tr>
        
                                                                        <tr>
                                                                            <td>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1<br>Total PAGE-1</td>
                                                                            <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                            <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                            <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                            <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                            <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                            <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                            <td>24512<br>24512<br>24512<br>24512<br>24512<br>24512<br>24512</td>
                                                                        </tr>
        
        
                                                                        <tr>
                                                                            <th>GRAND TOTAL RS</th>
                                                                            <th>124589</th>
                                                                            <th></th>
                                                                            <th>10458</th>
                                                                            <th></th>
                                                                            <th>02582</th>
                                                                            <th>0</th>
                                                                            <th>1245877</th>
                                                                        </tr>
        
                                                                    </table>
                                                                    <p style="text-align: center;"> (Rupees one lakh forty Thousand one Hundred seventy Six Only)</p>
        
                                                                    <p style="text-align: end;">Asst Exce<br>Civ Est Office<br>Stn Wksp EME Delhi Cantt-10</p>
        
        
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="arrear_da_nps" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs" role="tablist">
                               
                                                <li class="nav-item ">
                                                    <a class="nav-link active" data-toggle="tab" href="#create_arr_nps" role="tab">Create Arrear (NPS)</a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link" data-toggle="tab" href="#ar_nps_list" role="tab">Arrear NPS List</a>
                                                </li>
                                                {{-- <li class="nav-item ">
                                                    <a class="nav-link" data-toggle="tab" href="#bill_pay" role="tab">Bill Pay </a>
                                                </li>
                                                <li class="nav-item d-none">
                                                    <a class="nav-link" data-toggle="tab" href="#emp_sal_slip" role="tab">Salary Slip </a>
                                                </li> --}}
                                                
                
                
                                            </ul><!-- Tab panes -->
                                            <div class="tab-content">
                               
                                                <div class="tab-pane active" id="create_arr_nps" role="tabpanel">

                                                </div>
                                                <div class="tab-pane " id="ar_nps_list" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col text-right">
                                                            <a href="javascript:printPdf('arrea_da_nps_div')" class="text-primary"><i
                                                                    class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="arrea_da_nps_div">
                                                        <style type="text/css" media="print">
                                                            body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                            @page { size: landscape; }
                                                            
                                                          </style>
        
        
                                                        <div class="col">
                                                            <div class="row">
                                                                <div class="col">
                                                                    
        
                                                                    <h5 style="text-align: center;"><u>STATION WORKSHOP EME, DELHI CANTT - 110010</u></h5>
                                                                    <H6 style="text-align: center;"><u>SY CHECK ROLL NO 50601/CIV/NPS/ DT OCT 2022: ARREAR OF DA FOR THE PERIOD JUL
                                                                            2022 TO SEP 2022</u></H6>
                                                                </div>   
                                                            </div>
                                                            <div class="row my-2">
                                                                <div class="col-md-12">
                                                                    <table class="w-100" border="1" style="font-size: 11px">
                                                                        <tr>
                                                                            <th>Ser</th>
                                                                            <th>T.No Trade & Name</th>
                                                                            <th>Month</th>
                                                                            <th>Basic Pay </th>
                                                                            <th>Month Days</th>
                                                                            <th>DA</th>
                                                                            <th>TA</th>
                                                                            <th>Arr of TA</th>
                                                                            <th>Arr of HRA</th>
                                                                            <th>Total (F+H+L)</th>
                                                                            <th>EOL Days</th>
                                                                            <th>EOL Rec</th>
                                                                            <th>Total (-i)</th>
                                                                            <th>GHC 14 % of DA</th>
                                                                            <th>Total Arrear Of DA</th>
                                                                            <th>GMC Rec</th>
                                                                            <th>CPS Rec</th>
                                                                            <th>Total Rec</th>
                                                                            <th>Amt payable(O-r)</th>
                                                                            <th>Signatu</th>
                                                                        </tr>
                                                            
                                                            
                                                                        <tr>
                                                                            <td>(a)</td>
                                                                            <td>(b)</td>
                                                                            <td>(c)</td>
                                                                            <td>(d)</td>
                                                                            <td>(e)</td>
                                                                            <td>(f)</td>
                                                                            <td>(g)</td>
                                                                            <td>(h)</td>
                                                                            <td>(i)</td>
                                                                            <td>(j)</td>
                                                                            <td>(k)</td>
                                                                            <td>(l)</td>
                                                                            <td>(m)</td>
                                                                            <td>(n)</td>
                                                                            <td>(o)</td>
                                                                            <td>(p)</td>
                                                                            <td>(q)</td>
                                                                            <td>(r)</td>
                                                                            <td>(s)</td>
                                                                            <td>(t)</td>
                                                            
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td>1.</td>
                                                                            <td>T.No 402 TCM Sh Dayal Singh</td>
                                                                            <td>Jul<br>Aug<br>Sep</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>0</td>
                                                                            <td>0</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                            
                                                            
                                                            
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>24589</td>
                                                                            <td>48795</td>
                                                                            <td>7854</td>
                                                                            <td>9857</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                                        <tr>
                                                                            <td>2.</td>
                                                                            <td>T.No 402 TCM Sh Dayal Singh</td>
                                                                            <td>Jul<br>Aug<br>Sep</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>54854<br>89455<br>24587</td>
                                                                            <td>0</td>
                                                                            <td>0</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                            
                                                            
                                                            
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>24589</td>
                                                                            <td>48795</td>
                                                                            <td>7854</td>
                                                                            <td>9857</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                            
                                                            
                                                            
                                                            
                                                                        <tr>
                                                                            <td></td>
                                                                            <td><b>TOTAL RS</b></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>489</td>
                                                                            <td>4789</td>
                                                                            <td>24589</td>
                                                                            <td>48795</td>
                                                                            <td>7854</td>
                                                                            <td>9857</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td>7895</td>
                                                                            <td>487</td>
                                                                            <td>558745</td>
                                                                            <td>4579</td>
                                                                            <td></td>
                                                                        </tr>
                                                            
                                                            
                                                            
                                                            
                                                                    </table>
                                                            
                                                            
                                                                    <table class="mt-1 w-100" border="1" style="font-size: 11px">
                                                                        <tr>
                                                                            <th>PAGE NO</th>
                                                                            <th>TOTAL INCL GMC</th>
                                                                            <th colspan="3">NPS</th>
                                                                            <th>TOTAL</th>
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td>PAGE 1<br>PAGE 2 <br> PAGE 3<br>PAGE 4 <br> PAGE 5<br>PAGE 6</td>
                                                                            <td>245844<br>54869<br>15897<br>24589<br>56987<br>589746<br>5428</td>
                                                                            <td>00000</td>
                                                                            <td>25489<br>58744<br>568974<br>897456<br>897456<br>58976<br>457896</td>
                                                                            <td>00000</td>
                                                                            <td>589745<br>45896<br>578946<br>589466<br>45894<br>58496</td>
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td><b>TOTAL RS</b></td>
                                                                            <td>2548975</td>
                                                                            <td colspan="3">3598745</td>
                                                                            <td>57894855</td>
                                                                        </tr>
                                                                    </table>
                                                            
                                                                    <h6 style="text-align: center;">(Rupees one Lakh forty one Thousand Two Hundred Eighty one only)</h6>
                                                            
                                                            
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="bill_pay" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs" role="tablist">
                               
                                                <li class="nav-item ">
                                                    <a class="nav-link " data-toggle="tab" href="#create_bill" role="tab">Create Bill</a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link active" data-toggle="tab" href="#pay_bill_list" role="tab">Paid Bills</a>
                                                </li>
                                                {{-- <li class="nav-item ">
                                                    <a class="nav-link" data-toggle="tab" href="#bill_pay" role="tab">Bill Pay </a>
                                                </li>
                                                <li class="nav-item d-none">
                                                    <a class="nav-link" data-toggle="tab" href="#emp_sal_slip" role="tab">Salary Slip </a>
                                                </li> --}}
                                                
                
                
                                            </ul><!-- Tab panes -->
                                            <div class="tab-content">
                               
                                                <div class="tab-pane " id="create_bill" role="tabpanel">
                                                    <form id="add-new-bill">
                                                        <div class="row">
                                                            <div class="col">
                                                                <label>Reg No.</label>
                                                                <input name="reg_no" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>Name</label>
                                                                <input name="name" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>Father's Name</label>
                                                                <input name="fname" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>DOB</label>
                                                                <input name="dob" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>Trade</label>
                                                                <input name="trade" class="form-control" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <label>DOA</label>
                                                                <input name="doa" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>P Days</label>
                                                                <input name="pdays" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>Days in Month</label>
                                                                <input name="dmonth" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>Stipend</label>
                                                                <input name="stpnd" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>Net Payable</label>
                                                                <input name="nt_py" class="form-control" type="text">
                                                            </div>
                                                            <div class="col">
                                                                <label>Remarks</label>
                                                                <input name="rem" class="form-control" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <hr>
                                                                <input type="submit" value="Submit" class="btn btn-success">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane active" id="pay_bill_list" role="tabpanel">
                                                    <ul class="nav nav-tabs" role="tablist">
                               
                                                        <li class="nav-item ">
                                                            <a class="nav-link active" data-toggle="tab" href="#update_able_list" role="tab">Update List</a>
                                                        </li>
                                                        <li class="nav-item ">
                                                            <a class="nav-link" data-toggle="tab" href="#final_list" role="tab">Final Bills</a>
                                                        </li>
                                                       
                                                        
                        
                        
                                                    </ul><!-- Tab panes -->
                                                    <div class="tab-content">
                               
                                                        <div class="tab-pane active " id="update_able_list" role="tabpanel">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <a href="javascript:void(0)" class="btn btn-danger">Add These Bills to Budget Report</a>
                                                                <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row mt-2">
                                                                <div class="col">
                                                                    <table class="table table-bordered " style="font-size: 11px">
                
                
                                                                        <tr>
                                                                            <th>Ser No.</th>
                                                                            <th>Reg No.</th>
                                                                            <th>Name</th>
                                                                            <th>Father's Name</th>
                                                                            <th>DOB</th>
                                                                            <th>Trade</th>
                                                                            <th>DOA</th>
                                                                            <th>P Days</th>
                                                                            <th>Days in Month</th>
                                                                            <th>Stipend in Rs</th>
                                                                            <th>Net Payab</th>
                                                                            <th>Remarks</th>
                                                                            <th>Action</th>
                                                                        </tr>
        
                                                                        @php
                                                                                $i=1;
                                                                                $total=0;
                                                                            @endphp
                                                                            @foreach ($apprBill as $bill)
                                                                            <tr>
                                                                                <td>{{$i++}}</td>
                                                                                <td>{{$bill->reg_no}}</td>
                                                                                <td>{{$bill->name}}</td>
                                                                                <td>{{$bill->fname}}</td>
                                                                                <td>{{$bill->dob}}</td>
                                                                                <td>{{$bill->trade}}</td>
                                                                                <td>{{$bill->doa}}</td>
                                                                                <td>{{$bill->pdays}}</td>
                                                                                <td>{{$bill->day_in_month}}</td>
                                                                                <td>{{$bill->stipend}}</td>
                                                                                <td>{{$bill->net_pay}}</td>
                                                                                <td>{{$bill->remarks}}</td>
                                                                                <td>
                                                                                    <a href="javascript:void(0)" class="text-info "><i
                                                                                        class="fa fa-edit" aria-hidden="true"></i></a>
                                                                                    <a href="javascript:void(0)" class="text-danger "><i
                                                                                        class="fa fa-trash" aria-hidden="true"></i></a>
                                                                                </td>
                                                                            </tr>
                                                                            @php
                                                                                $total+=$bill->net_pay;
                                                                            @endphp
                                                                            @endforeach
                                                                      
                                                                        <tr>
                                                                            <td colspan="5"></td>
                                                                            <td colspan="5">Total Rs</td>
                                                                            <td>{{$total}}</td>
                                                                            <td colspan="2"></td>
                                                                        </tr>
        
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="tab-pane " id="final_list" role="tabpanel">
                                                            
                                                            <div class="row">
                                                                <div class="col text-right">
                                                                    <a href="javascript:printPdf('pay_bill_div')" class="text-primary"><i
                                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="pay_bill_div">
                                                                <style type="text/css" media="print">
                                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                                    @page { size: landscape; }
                                                                    
                                                                  </style>
                
                
                                                                <div class="col">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            
                
                                                                            <h5 style="text-align: center;" class="mt-5"><u>STATION WORKSHOP EME, DELHI CANTT-110010</u></h5>
                                                                            <h6 style="text-align: center;"><u>CHECK ROLL (PAY BILL) IN R/O APPRENTICESHIP CANDIDATE THE MONTH OF OCT
                                                                                    2022</u></h6>
                                                                        </div>   
                                                                    </div>
                                                                    <div class="row my-2">
                                                                        <div class="col-md-12">
                                                                            
                                                                                <table class="w-100" border="1" style="font-size: 11px">
                
                
                                                                                    <tr>
                                                                                        <th>Ser No.</th>
                                                                                        <th>Reg No.</th>
                                                                                        <th>Name</th>
                                                                                        <th>Father's Name</th>
                                                                                        <th>DOB</th>
                                                                                        <th>Trade</th>
                                                                                        <th>DOA</th>
                                                                                        <th>P Days</th>
                                                                                        <th>Days in Month</th>
                                                                                        <th>Stipend in Rs</th>
                                                                                        <th>Net Payab</th>
                                                                                        <th>Remarks</th>
                                                                                    </tr>
                
                                                                                    @php
                                                                                            $i=1;
                                                                                            $total=0;
                                                                                        @endphp
                                                                                        @foreach ($apprBill as $bill)
                                                                                        <tr>
                                                                                            <td>{{$i++}}</td>
                                                                                            <td>{{$bill->reg_no}}</td>
                                                                                            <td>{{$bill->name}}</td>
                                                                                            <td>{{$bill->fname}}</td>
                                                                                            <td>{{$bill->dob}}</td>
                                                                                            <td>{{$bill->trade}}</td>
                                                                                            <td>{{$bill->doa}}</td>
                                                                                            <td>{{$bill->pdays}}</td>
                                                                                            <td>{{$bill->day_in_month}}</td>
                                                                                            <td>{{$bill->stipend}}</td>
                                                                                            <td>{{$bill->net_pay}}</td>
                                                                                            <td>{{$bill->remarks}}</td>
                                                                                        </tr>
                                                                                        @php
                                                                                            $total+=$bill->net_pay;
                                                                                        @endphp
                                                                                        @endforeach
                                                                                  
                                                                                    <tr>
                                                                                        <td colspan="5"></td>
                                                                                        <td colspan="5">Total Rs</td>
                                                                                        <td>{{$total}}</td>
                                                                                        <td></td>
                                                                                    </tr>
                
                                                                                </table>
                
                
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                   
                                                </div>
                                            </div>




                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
