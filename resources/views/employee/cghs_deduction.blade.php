@extends('employee.layouts.main')
@section('main')
    <style>
       
    </style>
    <!-- end topbar -->
    <!-- dashboard inner -->
    <div class="midde_cont mt-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <span class="font-weight-bold ">Employee CGHS Deductions

                                @if ($id!=null)
                                    <span class="text-success">{{$data->name}} - {{$data->emp_id}}</span>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" role="tablist">
                               
                                <li class="nav-item ">
                                    <a class="nav-link <?= $id==null ? 'active' : 'd-none' ?>" data-toggle="tab" href="#empList" role="tab">Employee List</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link  <?= $id!=null ? 'active' : 'd-none' ?>" data-toggle="tab" href="#deducted_cghs" role="tab">Deducted CGHS </a>
                                </li>
                               
                                


                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                               @if ($id==null)
                                <div class="tab-pane <?= $id==null ? 'active' : '' ?>" id="empList" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <table class="table table-bordered myTable ">
                                                <thead>
                                                    <th class="text-center">S.No</th>
                                                    <th class="text-center">Emp ID</th>
                                                    <th class="text-center">Emp Name</th>
                                                    <th class="text-center">Date of Joining</th>
                                                    <th class="text-center">Date of Retirement</th>
                                                    <th class="text-center">Gender</th>
                                                    <th class="text-center">Mobile No</th>
                                                    <th class="text-center">Alternative No</th>
                                                    
                                                </thead>
                
                                                <tbody class="text-center">
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($data as $d)
                                                    
                                                        <tr>
                                                            <td>{{$i++}}</td>
                                                            <td><a href="{{route('view-cghs-deductions',$d->emp_id)}}" class="text-primary">{{$d->emp_id}}</a></td>
                                                            <td>{{$d->name}}</td>
                                                            <td>{{$d->do_joining}}</td>
                                                            <td>{{$d->do_retirement}}</td>
                                                            
                                                            <td>{{$d->gender}}</td>
                                                            
                                                            <td>{{$d->primary_mob}}</td>
                                                            <td>{{$d->alternat_mob}}</td>
                                                        
                                                        </tr>
                                                    
                                                    @endforeach
                
                                                </tbody>
                
                
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                               @endif
                               
                                @if ($id!=null)
                                <div class="tab-pane <?= $id!=null ? 'active' : '' ?>" " id="deducted_cghs" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">

                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('arrear_da_div')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row mt-3" id="arrear_da_div">
                                                <style type="text/css" media="print">
                                                    /* body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}    */
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            <h4 style="text-align: center;">Min of Def letter No F17(5)78 C/D (Civ)-11 Dt 16 Dec 80 & 17 (5)/80/d(Civ) dt 13 Sep 1989 </h4>

                                                            <div class="row mt-5">
                                                                <div class="col-md-4">
                                                                    <h5>T.No {{!empty($data->t_no) ? $data->t_no : 'NA'}}</h5>
                                                                    <h5>DOB: {{!empty($data->dob) ? date('d M Y',strtotime($data->dob)) : 'NA'}}</h5>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <h5>Trade : {{!empty($data->trade) ? $data->trade : 'NA'}}</h5>
                                                                    <h5>DOE : 10 Sep 2021</h5>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <h5>Name: {{!empty($data->name) ? $data->name : 'NA'}}</h5>
                                                                    <h5>DOR: {{!empty($data->do_retirement) ? date('d M Y',strtotime($data->do_retirement)) : 'NA'}} </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="w-100" border="1">
                                                                <thead>
                                                                    <th class="text-center">Year</th>
                                                                    {{-- <th class="text-center">Emp ID</th>
                                                                    <th class="text-center">Emp Name</th> --}}
                                                                    @for ($i = 1; $i <= 12; $i++)
                                                                        @php
                                                                       
                                                                            switch($i){
                                                                                case 1:$month="Jan";break;
                                                                                case 2:$month="Feb";break;
                                                                                case 3:$month="Mar";break;
                                                                                case 4:$month="Apr";break;
                                                                                case 5:$month="May";break;
                                                                                case 6:$month="Jun";break;
                                                                                case 7:$month="Jul";break;
                                                                                case 8:$month="Aug";break;
                                                                                case 9:$month="Sep";break;
                                                                                case 10:    $month="Oct";break;
                                                                                case 11:    $month="Nov";break;
                                                                                case 12:$month="Dec";break;
                                                                               
                                                                            }
                                                                        @endphp
                                                                        <th class="text-center">{{$month}}</th>
                                                                    @endfor
                                                                </thead>
                                
                                                                <tbody class="text-center">
                                                                   @for ($y = date('Y',strtotime($data->do_joining)); $y < date('Y'); $y++)
                                                                       <tr>
                                                                            <td>{{$y}}</td>
                                                                            
                                                                            @for ($k = 1; $k <= 12; $k++)
                                                                            @php
                                                                                $amt=0;
                                                                            @endphp
                                                                                @foreach ($data->cghsdeductions as $item)
                                                                                    @if ($item->month== $k && $item->year==$y)
                                                                                        @php
                                                                                            $amt=$item->amount;
                                                                                        @endphp 
                                                                                    @endif
                                                                                @endforeach
                                                                                <td class="text-center">{{$amt}}</td>
                                                                            @endfor
                                                                       </tr>
                                                                   @endfor
                                
                                                                </tbody>
                                
                                
                                                            </table>

                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                @endif
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- row -->


        </div>
    </div>
@endsection
