@extends('employee.layouts.main')
@section('main')
    <!-- end topbar -->
    <!-- dashboard inner -->
    <div class="midde_cont mt-2">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="col text-left">
                            
                            <label class="font-weight-bold"> Employee CGEIS Deductions</label>
                               
                        </div>
                        <div class="col text-right d-none">
                            <a href="javascript:void(0)" id="generateEmpSalary" class="btn btn-info">Generate Salary</a>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row my-1">
                <style>
                    .tdd{
                        width:20px;
                        height: 20px;
                    }
                    input {
                        border-top-style: hidden;
                        border-right-style: hidden;
                        border-left-style: hidden;
                        border-bottom-style: groove;
                        /* background-color: #eee; */
                        }

                        .no-outline:focus {
                        outline: none;
                        }
                </style>
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#empl_list" role="tab">Employee List</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" data-toggle="tab" href="#emp_cgeis_Dec" role="tab">CGEIS Deduction</a>
                                </li>
                               


                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                               
                                <div class="tab-pane active " id="empl_list" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            
                                            <div class="row" id="arrear_da_div">
                                               
                                                <div class="col">
                                                    <table class="table table-bordered myTable ">
                                                        <thead>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Emp ID</th>
                                                            <th class="text-center">Emp Name</th>
                                                            <th class="text-center">Date of Joining</th>
                                                            <th class="text-center">Date of Retirement</th>
                                                            <th class="text-center">Gender</th>
                                                            <th class="text-center">Mobile No</th>
                                                            <th class="text-center">Alternative No</th>
                                                            
                                                        </thead>
                        
                                                        <tbody class="text-center">
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($data as $d)
                                                              
                                                                <tr>
                                                                    <td>{{$i}}</td>
                                                                    <td><a href="{{route('cgeis-deduction',$d->emp_id)}}" class="text-primary">{{$d->emp_id}}</a></td>
                                                                    <td>{{$d->name}}</td>
                                                                    <td>{{$d->do_joining}}</td>
                                                                    <td>{{$d->do_retirement}}</td>
                                                                    
                                                                    <td>{{$d->gender}}</td>
                                                                    
                                                                    <td>{{$d->primary_mob}}</td>
                                                                    <td>{{$d->alternat_mob}}</td>
                                                                   
                                                                   
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
                        
                                                        </tbody>
                        
                        
                                                    </table>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="emp_cgeis_Dec" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <a href="javascript:printPdf('cgeis_deduc')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="cgeis_deduc">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>


                                                <div class="col">
                                                    
                                                    <div class="row my-2">
                                                        <div class="col-md-12">
                                                            <h4 style="text-align: center;">Min of Def letter No F17(5)78 C/D (Civ)-11 Dt 16 Dec 80 & 17 (5)/80/d(Civ) dt 13 Sep 1989 </h4>

                                                            <div class="row mt-5">
                                                                <div class="col-md-4">
                                                                    <h5>T.No 227</h5>
                                                                    <h5>DOB:  02 May 1962</h5>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <h5>Trade : T/Mate</h5>
                                                                    <h5>DOE : 10 Sep 2021</h5>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <h5>Name: Smt Bimla Devi</h5>
                                                                    <h5>DOR: 31 May 2022</h5>
                                                                </div>
                                                            </div>
                                                            <div class="row mt-2">
                                                                <div class="col">
                                                                    <table class="w-100" border="1" style="text-align: center;">
                                                                        <tr >
                                                                            <th>Months</th>
                                                                            <th>Jan</th>
                                                                            <th>Feb</th>
                                                                            <th>Mar</th>
                                                                            <th>Apr</th>
                                                                            <th>May</th>
                                                                            <th>Jun</th>
                                                                            <th>Jul</th>
                                                                            <th>Aug</th>
                                                                            <th>Sep</th>
                                                                            <th>Oct</th>
                                                                            <th>Nov</th>
                                                                            <th>Dec</th>
                                                                            <th>Total</th>
        
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2002</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>1254</td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2002</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>1254</td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2002</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>15</td>
                                                                            <td>1254</td>
                                                                            
                                                                        </tr>
        
                                                                        <tr>
                                                                            <th>Total</th>
                                                                            <th>545</th>
                                                                            <th>8755</th>
                                                                            <th>8955</th>
                                                                            <th>545</th>
                                                                            <th>8755</th>
                                                                            <th>8955</th>
                                                                            <th>545</th>
                                                                            <th>8755</th>
                                                                            <th>8955</th>
                                                                            <th>545</th>
                                                                            <th>8755</th>
                                                                                <th>8955</th>
                                                                                <th>545</th>
                                                                            
                                                                            
                                                                        </tr>
        
        
        
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
