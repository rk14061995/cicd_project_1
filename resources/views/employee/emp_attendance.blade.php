@extends('employee.layouts.main')
@section('main')
    <style>
       
    </style>
    <!-- end topbar -->
    <!-- dashboard inner -->
    <div class="midde_cont py-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <span class="font-weight-bold ">Mark Employee Attendace</span>
                            {{-- <hr> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-1">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Check IN</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Check OUT</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#attenSheet" role="tab">Attendace Sheet</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#attenList" role="tab">Attendace History</a>
                                </li>

                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                    <form id="markAttendance">
                                        <div class="row">
                                            <div class="col">
                                                {{-- <label>Employee</label>// --}}
                                                <select class="form-control empSearch" style="width:100%" name="empId">
                                                    <option value="all">All</option>
                                                    {{-- <option value="0">Select Employee</option> --}}
                                                    @foreach ($Employees as $emp)
                                                        <option value="{{ $emp->emp_id }}">{{ $emp->name }}
                                                            ({{ $emp->emp_id }}) </option>
                                                    @endforeach
        
                                                </select>
                                            </div>
                                            <div class="col">
                                                <input type="date" name="date"  id="mark_empAttnDate" 
                                                    class="form-control">
                                            </div>
                                            <div class="col">
                                                {{-- <label>Time</label> --}}
                                                <input type="time" name="time" id="mark_empAttnTime" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                {{-- <label> </label><br> --}}
                                                <input type="submit" value="Mark Attendance - Check IN" class="btn btn-success"
                                                    style="width:100%;heigh:100%">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane" id="tabs-2" role="tabpanel">
                                    <form id="markAttendance">
                                        <div class="row">
                                            
                                            <div class="col">
                                                {{-- <label>Employee</label>// --}}
                                                <input type="hidden" name="attendId" id="attendId" value="all"
                                                    class="form-control">
                                                <select class="form-control empSearch" id="empCheckoutID" style="width:100%" name="empId">
                                                    {{-- <option value="0">Select Employee</option> --}}
                                                    <option value="all">All</option>
                                                    @foreach ($dataOut as $emp)
                                                        <option value="{{ $emp->id }}">{{ $emp->empdetails->empName }}
                                                            ({{ $emp->emp_id }}) </option>
                                                    @endforeach
        
                                                </select>
                                            </div>
                                            <div class="col">
                                                <input type="date" name="date" id="mark_checkOutAttnd"
                                                    class="form-control">
                                                    
                                            </div>
                                            <div class="col">
                                                {{-- <label>Time</label> --}}
                                                <input type="time" name="time" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                {{-- <label> </label><br> --}}
                                                <input type="submit" value="Mark Attendance - Check OUT" class="btn btn-danger"
                                                    style="width:100%;heigh:100%">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane " id="attenList" role="tabpanel">
                                    <table class="table table-bordered myTable " style="font-size: 11px">
                                        <thead>
                                            <th class="text-center">S.No</th>
                                            <th class="text-center">Emp ID</th>
                                            <th class="text-center">Emp Name</th>
                                            <th class="text-center">Attendance Date</th>
                                            <th class="text-center">Attendance In-Time</th>
                                            <th class="text-center">Attendance Out-Time</th>
                                            <th class="text-center">Working Time</th>
                                            <th class="text-center">Attendance Status</th>
                                            <th class="text-center">Action</th>
                                            {{-- <th>Designation</th>
                                        <th>Address</th> --}}
                                            {{-- <th>Mobile No.</th> --}}
                                        </thead>
        
                                        <tbody class="text-center">
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($data as $d)
                                                @php
                                                    // $empD=$d->empdetails;
                                                    // echo '<pre>';
                                                    // print_r($d->toArray());
                                                @endphp
                                                <tr>
                                                    @php
                                                        $time1 = new DateTime($d->attendance_time_in);
                                                        if(!empty($d->attendance_time_out)){
                                                            $hck=$d->attendance_time_out;
                                                        }else{
                                                            $hck='06:00:00 PM';
                                                        }
                                                        $time2 = new DateTime($hck);
                                                        $interval = $time1->diff($time2);
                                                        // print_r($interval);
                                                        
                                                    @endphp     
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $d->emp_id }}</td>
                                                    <td>{{ $d->empdetails->empName }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($d->attendance_date)) }}</td>
                                                    <td>{{ date('h:i:s A', strtotime($d->attendance_time_in)) }}</td>
                                                    <td>@if (!empty($d->attendance_time_out))
                                                            {{date('h:i:s A', strtotime($d->attendance_time_out)) }}
                                                        @else
                                                            {{date('h:i:s A', strtotime('06:00:00 PM')) }}
                                                        @endif
                                                    </td>
                                                    <td>{{  $interval->format('%h h - %i m')}}</td>
                                                    <td>
        
                                                        @if ($d->absent_present == 1)
                                                            <span class="text-success">Present</span>
                                                        @else
                                                            <span class="text-danger">Absent</span>
                                                        @endif
                                                        {{-- @if ($d->absent_present == 1) --}}
                                                        @php
                                                            $atnArray = ['1', '0'];
                                                            // echo $d->absent_present = rand(1, 0);
                                                        @endphp
                                                        <select class="form-control text-center d-none" name="changeAttendance"
                                                            id="changeAttendance">
                                                            @foreach ($atnArray as $atn)
                                                                <?php
                                                                $atnStatus = ' ';
                                                                ?>
        
                                                                @if ($d->absent_present == $atn)
                                                                    {{ $atnStatus }}
                                                                    @if ($atn == 1)
                                                                        @php
                                                                            $atnStatus = 'Present';
                                                                        @endphp
                                                                    @else
                                                                        @php
                                                                            $atnStatus = 'Absent';
                                                                        @endphp
                                                                    @endif
                                                                    <option value="{{ $atn }}" selected>
                                                                        {{ $atnStatus }}
                                                                    </option>
                                                                @else
                                                                    @if ($atn == 1)
                                                                        @php
                                                                            $atnStatus = 'Present';
                                                                        @endphp
                                                                    @else
                                                                        @php
                                                                            $atnStatus = 'Absent';
                                                                        @endphp
                                                                    @endif
                                                                    <option value="{{ $atn }}">{{ $atnStatus }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                            {{-- <option value="0">Absent</option> --}}
                                                        </select>
                                                    </td>
                                                    {{-- <td>9721181225</td> --}}
                                                    <td>
                                                        <a href="javascript:void(0)" class="text-danger deleteEmpAttendanceData" data-id="{{$d->id}}"><i
                                                                class="fa fa-trash" aria-hidden="true"  ></i></a>
                                                    </td>
                                                </tr>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
        
                                        </tbody>
        
        
                                    </table>
                                </div>
                                <div class="tab-pane "  id="attenSheet" role="tabpanel">
                                    @php
                                        $totalDays=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime("-1 months")),date('Y',strtotime("-1 months")));
                                        // $number = cal_days_in_month(CAL_GREGORIAN, 8, 2003); // 31
                                        // echo "There were ".$number ."days in August 2003";
                                    @endphp                             
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col tex-right">
                                                    <a href="javascript:printPdf('dayWisePayRole')" class="text-primary"><i
                                                            class="fa fa-print" aria-hidden="true"></i> Print</a>
                                                </div>
                                            </div>
                                            <div class="row" id="dayWisePayRole">
                                                <style type="text/css" media="print">
                                                    body{margin: 10px 30px 10px 30px;padding:10px 20px 50px 10px;}   
                                                    @page { size: landscape; }
                                                    
                                                  </style>
                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            <h3 style="text-align: center;" class="mt-5"><u>STATION WORKSHOP EME, DELHI CANTT - 10 </u></h3>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <h6> OF INDUSTRIAL CIVILIANS FOR THE MONTH OF {{date('M').' '.date('Y')}}</h6>
                                                        </div>
                                                        <div class="col">
                                                            <p style="text-align: end;">Working Days = {{$wd}}
                                                                
                                                                <br>
                                                                {{date('M').' '.date('Y')}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <table class="w-100" border="1" style="font-size: 12px">
                                                                
        
                                                                <tr>
                                                                    <th>Trade & Name</th>
                                                                    @for($i = 1; $i <= $totalDays; $i++)
                                                                        <th class="text-center tdd">{{$i}}</th>
                                                                    @endfor
                                                                    
                                                                    <th class="text-center ">DO II No.</th>
                                                                </tr>
            
            
                                                                @php
                                                                     $d=1;
                                                                 @endphp
                                                                    @foreach ($attenArray as $empSal)
                                                                        <tr>
                                                                            <td>({{$empSal['trade']}}) {{$empSal['name']}}</td>
                                                                            @php
                                                                                $weekend=false;
                                                                                $nmm=0;
                                                                            @endphp
                                                                            @for ($i = 0; $i < $totalDays; $i++)
                                                                                @php

                                                                                    $da=$i+1;
                                                                                  
                                                                                    $year=date('Y');
                                                                                    $month=date('m');
                                                                                    $day=$da;
                                                                                     $startdate = strtotime($year . '-' . $month .'-'.$day);
                                                                                    if(date('D',$startdate)=='Sun'){
                                                                                        $weekend=true;
                                                                                        // echo 'TRUE';
                                                                                    }else{
                                                                                        $weekend=false;
                                                                                    }


                                                                                //    echo date('D',strtotime());
                                                                                @endphp
                                                                                @if (isset($empSal['currentleaves']) && count($empSal['currentleaves'])>0)
                                                                                    @php
                                                                                        $onLeave=false;
                                                                                        $ct="";
                                                                                    @endphp
                                                                                    @foreach ($empSal['currentleaves'] as $item)
                                                                                        @php
                                                                                    
                                                                                            $from=date('d',strtotime($item['from']));
                                                                                            $to=date('d',strtotime($item['to']));
                                                                                            for($j=$from; $j<=$to; $j++){
                                                                                                if($j==$da){
                                                                                                    $onLeave=true;
                                                                                                    $nmm=2;
                                                                                                    $ct=$item['category'];
                                                                                                }
                                                                                            }
                                                                                            
                                                                                        @endphp
                                                                                    
                                                                                    @endforeach
                                                                                    
                                                                                @else
                                                                                    @php
                                                                                        $onLeave=false;
                                                                                        // $attenSt='A';
                                                                                    @endphp
                                                                                @endif 
                                                                                @if (isset($empSal['currentattendance']) && count($empSal['currentattendance'])>0)
                                                                                    @php
                                                                                        $presentTrue=false;
                                                                                    @endphp
                                                                                    @foreach ($empSal['currentattendance'] as $item)
                                                                                    @php
                                                                                        $day=date('d',strtotime($item['attendance_date']));
                                                                                        if($day==$da){
                                                                                            $presentTrue=true;  
                                                                                        }
                                                                                        if(date('d') < $da){
                                                                                            $nmm=2;
                                                                                        }
                                                                                        
                                                                                    @endphp
                                                                                      
                                                                                    @endforeach
                                                                                    
                                                                                @else
                                                                                    @php
                                                                                        if(date('d')<$da && $presentTrue==false){
                                                                                            $nmm=2;
                                                                                        }else{
                                                                                            $presentTrue=false;
                                                                                        }
                                                                                        
                                                                                        // $attenSt='A';
                                                                                    @endphp
                                                                                @endif
                                                                               
                                                                                @php
                                                                                if($weekend){
                                                                                    $black='bg-dark';
                                                                                    $attenSt='';
                                                                                    $class='';
                                                                                }else{
                                                                                    $black='';
                                                                                    if($onLeave){
                                                                                        $attenSt=$ct;
                                                                                        $class="text-success font-weight-bold";
                                                                                        
                                                                                    }else{
                                                                                       
                                                                                        if($presentTrue){
                                                                                            $attenSt='P';
                                                                                            $class="text-primary font-weight-bold";
                                                                                        }elseif($nmm==2){
                                                                                            $attenSt='';
                                                                                            $class="";
                                                                                        }else{
                                                                                            $attenSt='A';
                                                                                            $class="text-danger";
                                                                                        }
                                                                                    }
                                                                                    // $black='';
                                                                                    // if($presentTrue){
                                                                                    //     $attenSt='P';
                                                                                    //     $class="text-primary font-weight-bold";
                                                                                    // }elseif($nmm==2){
                                                                                    //     $attenSt='';
                                                                                    //     $class="";
                                                                                    // }else{
                                                                                    //     $attenSt='A';
                                                                                    //     $class="text-danger";
                                                                                    // }
                                                                                }
                                                                                    
                                                                                @endphp
                                                                                

                                                                                <td class="tdd text-center {{$class}} {{$black}}">{{$attenSt}}</td>
                                                                                
                                                                            @endfor
                                                                            <td class=" text-center">-do- 1254255254</td>
                                                                        </tr>
                                                                    @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>  
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
            <div class="row d-none">
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <span class="font-weight-bold ">Employee Attendance History</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-1 d-none">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            
                        </div>
                    </div>
                </div>
            </div>

            <!-- row -->


        </div>
    </div>
@endsection