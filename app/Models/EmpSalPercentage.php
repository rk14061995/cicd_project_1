<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpSalPercentage extends Model
{
    use HasFactory;
    protected $table="emp_sal_percentage";
}
