<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class EmployeeLeave extends Model
{
    use HasFactory;
    protected $table="employee_leave";
    public function empdetails(){
        return $this->belongsTo('App\Models\User','emp_id','emp_id');
    }
    public function inchDtl(){
        return $this->belongsTo('App\Models\User','empInchgId','emp_id');
    }
    public static function getEmpLeaveDetails($category,$empId){
        // if($finYear==null){
            return DB::table('employee_leave')->where('employee_leave.emp_id',$empId)->where('category',$category)->get();
        // }
    }
}
