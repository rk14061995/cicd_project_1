<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NPSDeduction extends Model
{
    use HasFactory;
    protected $table="emp_nps_deduction";
}
