<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApprenticePaysModel extends Model
{
    use HasFactory;
    protected $table="apprentice_pays";
}
