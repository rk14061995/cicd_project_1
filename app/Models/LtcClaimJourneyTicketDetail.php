<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LtcClaimJourneyTicketDetail extends Model
{
    use HasFactory;
    protected $table="ltc_claim_journey_ticket_detail";
    protected $fillable = [
        'emp_id','passenger_name','passender_rel','depend_id', 'doc_file_passangr'
      ];
}
