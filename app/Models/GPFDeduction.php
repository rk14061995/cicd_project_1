<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GPFDeduction extends Model
{
    use HasFactory;
    protected $table="emp_gpf_deduction";
}
