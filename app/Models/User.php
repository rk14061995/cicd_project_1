<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function empSalarydetails(){
        return $this->belongsTo('App\Models\EmployeeSalary', 'emp_id', 'emp_id')
                    // ->where('status', 1)
                    ->where('salary_status', 1)
                    ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    
    public function attendance(){
        return $this->hasMany('App\Models\EmployeeAttendanceModel', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->where('attendance_date','>',date('Y-m-1',strtotime('-1 months')))
                    ->where('attendance_date','<',date('Y-m-1'))
                    ->select('eol_h','attendance_date','attendance_time_in','attendance_time_out','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    public function currentattendance(){
        return $this->hasMany('App\Models\EmployeeAttendanceModel', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->where('attendance_date','>', date('Y-m-01'))
                    ->where('attendance_date','<',date('Y-m-01', strtotime('+1 months')))
                    ->select('attendance_date','attendance_time_in','attendance_time_out','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    public function currentleaves(){
        return $this->hasMany('App\Models\EmployeeLeave', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->where('req_status',1)
                    ->where('from','>',date('Y-m-01'))
                    ->where('to','<',date('Y-m-01', strtotime('+1 months')))
                    ->select('from','to','category','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    public function leaves(){
        return $this->hasMany('App\Models\EmployeeLeave', 'emp_id', 'emp_id')
                    ->where('employee_leave.status', 1)
                    ->where('req_status',1)
                    ->where('from','>',date('Y-m-01',strtotime('-1 months')))
                    ->where('to','<',date('Y-m-01'))
                    ->join('leave_category','leave_category.category_name','=','employee_leave.category')
                    ->select('from','to','category','emp_id','leave_category.leave_type');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    public function cghsdeductions(){
        return $this->hasMany('App\Models\CghsDeduction', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->select('amount', 'month','year','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    public function cgeisdeductions(){
        return $this->hasMany('App\Models\CgeisDeduction', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->select('amount', 'month','year','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    public function gpfdeductions(){
        return $this->hasMany('App\Models\GPFDeduction', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->select('amount', 'month','year','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    public function npsdeductions(){
        return $this->hasMany('App\Models\NPSDeduction', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->select('amount', 'month','year','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
    
    public function post(){
        return $this->hasOne('App\Models\EmployeeSalary', 'emp_id', 'emp_id');
    }

}
