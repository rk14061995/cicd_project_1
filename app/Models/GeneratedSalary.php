<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneratedSalary extends Model
{
    use HasFactory;
    protected $table="generated_salary";
    public function empdetails(){
        // return $this->hasOne('App\Models\Manager\Employeedetails', 'id', 'emp_id');
        return $this->belongsTo('App\Models\User', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    ->select('id', 'name', 'emp_id','status','t_no','trade','do_retirement','aadhar_no','pan_no','gpf_no','doa','dob')->withDefault();
    }
    public function attendance(){
        return $this->hasMany('App\Models\EmployeeAttendanceModel', 'emp_id', 'emp_id')
                    ->where('status', 1)
                    // ->where('attendance_date','>',date('Y-m-1',strtotime('-1 months')))
                    ->where('attendance_date','>',date('Y-m-1',strtotime('-1 months')))
                    ->where('attendance_date','<',date('Y-m-1'))
                    ->select('attendance_date','attendance_time_in','attendance_time_out','emp_id');
                    // ->select('id','emp_id','basic_pay', 'ta_da', 'hra', 'tpt', 'pers_pay', 'govt_perks','salary_status');
    }
}
