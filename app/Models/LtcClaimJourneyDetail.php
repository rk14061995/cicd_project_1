<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LtcClaimJourneyDetail extends Model
{
    use HasFactory;
    protected $table="ltc_claim_journey_detail";
    protected $fillable = [
         'emp_id','mode_of_travel','date_of_journey','booking_date', 'departure_time', 'arrival_time', 'arrival_dat','passnger_no','source_stat','dest_stat','dept_time'
       ];
 
}
