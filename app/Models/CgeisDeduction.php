<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CgeisDeduction extends Model
{
    use HasFactory;
    protected $table="emp_cgeis_deduction";    
}
