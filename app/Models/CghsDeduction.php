<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class CghsDeduction extends Model
{
    use HasFactory;
    protected $table="emp_cghs_deduction";
}
