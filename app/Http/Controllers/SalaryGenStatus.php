<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmpSalaryDeduction;

class SalaryGenStatus extends Controller
{
    //
    public function addEmpDeductions(Request $req)
   {

      $response['status'] = false;
      $response['message'] = 'Something went wrong';
      if (!empty($req->input('empId'))) {
         $condition = array('emp_id' => $req->input('empId'));
         $res = EmpSalaryDeduction::where($condition)->get();
         if (count($res) > 0) {
            // $response['message'] = 'Pension Request Already Exists';
            //Update
            $data = array(
               'cghs' => $req->input('cghs'),
               'cgeis' => $req->input('cgeis'),
               'rent_rec' => $req->input('rent_Rec'),
               'gpf_ref' => $req->input('gpf'),
               'f_adv' => $req->input('f_adv'),
               "misc"=>$req->input('misc'),
               'tax_deduction' => $req->input('tax'),
               'nps_deduction' => $req->input('nps'),
            );
            // print_r($data);die;/
            if (EmpSalaryDeduction::where('emp_id',$req->input('empId'))->update($data)) {
               // Session::put('attendance',true);
               $response['status'] = true;
               $response['message'] = 'Employee Salary Deduction Details Updated Successfully';
               return ($response);
            } else {
               $response['message'] = 'Failed to Update Salary Deduction Details';
               return ($response);
            }
         } else {
            $data = array(
               'emp_id' => $req->input('empId'),
               'cghs' => $req->input('cghs'),
               'cgeis' => $req->input('cgeis'),
               'rent_rec' => $req->input('rent_Rec'),
               'gpf_ref' => $req->input('gpf'),
               'f_adv' => $req->input('f_adv'),
               "misc"=>$req->input('misc'),
               'tax_deduction' => $req->input('tax'),
               'nps_deduction' => $req->input('nps')
            );
            // print_r($data);die;/
            if (EmpSalaryDeduction::insert($data)) {
               // Session::put('attendance',true);
               $response['status'] = true;
               $response['message'] = 'Employee Salary Deduction Added Successfully';
               return ($response);
            } else {
               $response['message'] = 'Failed to Add  Salary Deduction Details';
               return ($response);
            }
            return response($response)->header('Content-Type', 'application/json');
         }  
         return response($response)->header('Content-Type', 'application/json');
      }
      return response($response)->header('Content-Type', 'application/json');
   } 
}
