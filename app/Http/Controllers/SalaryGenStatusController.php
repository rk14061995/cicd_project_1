<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalaryGenStatus;
use App\Models\User;
use App\Models\TransactionDetails;
use App\Models\TableDeduction;
use App\Models\EmpBankDetailsModel;
use App\Models\EmployeeSalary;
use App\Models\EmpSalaryDeduction;
use App\Models\FinancialYear;
use App\Models\GeneratedSalary;
use App\Models\EmpSalPercentage;
use App\Models\EmployeeAttendanceModel;
use App\Models\EmployeeLeave;

class SalaryGenStatusController extends Controller
{
    //
    function generateSalary()
    {
        $response['status'] = false;
        $response['message'] = 'Something went wrong';
        $currentFinancialYear = FinancialYear::where('current_status', 1)->first();
        // print_r($currentFinancialYear);die;
        $yearF = 0;
        if ($currentFinancialYear) {
            $yearF = $currentFinancialYear->financial_year;
        } else {
            $yearF = date('Y', strtotime('-1 years')) . date('Y');
        }
        $employee = User::where('users.status', 1)->with('attendance','leaves')
            // ->where('attendace_date',)
            ->join('emp_salary_deduction', 'emp_salary_deduction.emp_id', '=', 'users.emp_id')
            ->join('emp_salary_details', 'emp_salary_details.emp_id', '=', 'users.emp_id')
            // ->leftjoin('emp_attendance','emp_attendance.emp_id','=','users.emp_id')
            ->select('users.emp_id', 'users.user_job_type','users.name', 'emp_salary_details.*', 'emp_salary_deduction.*')
            ->get();
        // print_r($employee->toArray());die;
        $lastMonth = date('m-Y', strtotime('-1 months'));
        $_lm = date('m', strtotime('-1 months'));
        $_lY = date('Y', strtotime('-1 months'));
        $wd = $this->calculateWorkingDaysInMonth($_lY, $_lm);
        $temp = 0;
        foreach ($employee as $emp) {


            // $totalSal=$emp->basic_pay +  $emp->ta_da + $emp->hra + $emp->tpt + $emp->pers_pay  + $emp->govt_perks + $emp->wa + $emp->misc;
            $totalSal=$emp->basic_pay +  $emp->ta_da;
            // echo 'Total Sal: '.$totalSal;
            // echo "\r\n";

            $empWorkDays=0;
            $genStatus = array(
                "emp_id" => $emp->emp_id,
                "for_month_year" => $lastMonth,
                "financial_year" => $yearF,
                "month"=>$_lm,
                "year"=>$_lY
            );

            
            

            //Attendance Deduction Logic
            $workingDays=30;
            if($emp->user_job_type =='IP'){
                $workingDays=$wd;
            }
            // echo 'Working Days: '.$workingDays;
            // echo "\r\n";
            $perDaySalary=$totalSal/$workingDays;
            // echo ' Per Day Sal '. $perDaySalary;
            // echo "\r\n";
            $eol_h=0;
            foreach($emp->attendance as $empAttd ){
                // echo ' EOL H -> '.$empAttd->eol_h;
                $empWorkDays++;
                if($empAttd->eol_h!=0){
                    $eol_h+=$empAttd->eol_h;
                }
                
            }
            // echo 'Emp Worked On : '.$empWorkDays.' Days';
            // echo "\r\n";
            //Leave Deduction Logic
            foreach($emp->leaves as $lv){
                // print_r($lv->toArray());
                $dates=$this->getBetweenDates($lv->from, $lv->to);
                $upWorkDays=0;
                foreach($dates as $dt){
                    $upWorkDays++;
                    //to check GH or RH
                }
                if($lv->leave_type=='PAID'){
                    // echo 'Paid';
                    $empWorkDays+=$upWorkDays;
                }else{
                    // echo 'Non-Paid';
                }
                // print_r($dates);
            }
            
            // echo 'Emp Worked On : '.$empWorkDays.' Days';
            // echo "\r\n";
            $non_paid_leaves=$workingDays-$empWorkDays;
            // echo 'Salary Will Deduct for : '.$non_paid_leaves.' Days';
            // echo "\r\n";
            $eolH=(int)(($totalSal/$workingDays)*$eol_h);
            $eolD=(int)($non_paid_leaves*$perDaySalary);
            // print_r($genStatus);

            
            
            // print_r($salaryArray);
            if (count(SalaryGenStatus::where($genStatus)->get()) == 0) {
                if (SalaryGenStatus::insert($genStatus)) {
                    // `emp_id`, `gen_sal_st_id`, ``, ``, ``, `da`, `hra`, `tpt`, `pers_pay`, `wa`, `misc`, 
                    // `eol_h`, `eol_d`, `gpf`, `gpf_ref`, `cgeis`, `cghs`, `f_adv`, `rent_rec`, `misc_ded`, `tax_ded`
                    // print_r($genStatus);
                    // echo "\r\n";
                    // print_r($emp->toArray());
                    $id = 1;

                    //Transaction 
                    // enum('GPF', 'EOL', 'PERS_PAY', 'TPT', 'SALARY', 'LTC', 'CEA', 'MEDICAL', 'NPS', 'HRA', 'OVER_TIME', 'TA_DA', 'GRADE_PAY', 'TRANSPORT_ALLOWANCE')
                    TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->basic_pay, 'paid_as' => 'SALARY', 'initiated_for' => 'GRADE_PAY']);
                    TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->hra, 'paid_as' => 'SALARY', 'initiated_for' => 'HRA']);
                    TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->ta_da, 'paid_as' => 'SALARY', 'initiated_for' => 'TA_DA']);
                    TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->tpt, 'paid_as' => 'SALARY', 'initiated_for' => 'TPT']);
                    TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->pers_pay, 'paid_as' => 'SALARY', 'initiated_for' => 'PERS_PAY']);
                    TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->gpf_ref, 'paid_as' => 'SALARY', 'initiated_for' => 'GPF']);


                    $salaryArray = array(

                        "month"=>$_lm,
                        "year"=>$_lY,
                        "emp_id" => $emp->emp_id,
                        "gen_sal_st_id" => 1,
                        "for_month_year" => $lastMonth,
                        "financial_year" => $yearF,
                        "basic_pay" => $emp->basic_pay,
                        "da" => $emp->ta_da,
                        "hra" => $emp->hra,
                        "tpt" => $emp->tpt,
                        "pers_pay" => $emp->pers_pay,
                        "wa" => $emp->govt_perks,
                        "misc" => 0,
                        "total_pay"=>$emp->basic_pay +  $emp->ta_da + $emp->hra + $emp->tpt + $emp->pers_pay  + $emp->govt_perks + $emp->wa + $emp->misc,
                        "eol_h" => $eolH,
                        "eol_d" => $eolD,
                        "gpf" => $emp->gpf_ref,
                        "gpf_ref" => $emp->gpf_ref,
        
                        "cgeis" => $emp->cgeis,
                        "cghs" => $emp->cghs,
                        "f_adv" => $emp->f_adv,
                        "rent_rec" => $emp->rent_rec,
                        "misc_ded" => $emp->misc,
                        "tax_ded" => $emp->tax_deduction,
                        "total_deduction"=>$eolH +  $eolD + $emp->cgeis + $emp->cghs + $emp->f_adv  + $emp->rent_rec + $emp->tax_deduction,
                    );
                    // print_r($salaryArray);
                    // ;
                    if (count(GeneratedSalary::where($salaryArray)->get()) == 0) {
                        GeneratedSalary::insert($salaryArray);
                        $temp = 1;
                    }
                }
            } else {
                // /echo 'Already Generated';
                $response['status'] = false;
                $response['message'] = 'Already Generated';
            }
            if ($temp == 1) {
                $response['status'] = true;
                $response['message'] = 'Salary Generated';
            }
            // echo "----------------------------------------------------------------------------------------------------------------------";
        }


        return ($response);
        // print_r($currentFinancialYear);
        // print_r($employee->toArray());die;
    }
    public function getMonthlySalary($grossPay, $workingDays)
    {
        return ($grossPay / 30) * $workingDays;
    }
    public function getMonthlyDeductedSalary($grossPay, $workingDays, $weekends)
    {
        $perDaySal = ($grossPay / 30);
        $onLeave = 30 - ($workingDays + $weekends);
        return $perDaySal * $onLeave;
    }
    function getBetweenDates($startDate, $endDate)
    {
        $rangArray = [];
            
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);
             
        for ($currentDate = $startDate; $currentDate <= $endDate; 
                                        $currentDate += (86400)) {
                                                
            $date = date('Y-m-d', $currentDate);
            $rangArray[] = $date;
        }
  
        return $rangArray;
    }
  
    // $dates = getBetweenDates('2021-11-01', '2021-11-10');
     
    // print_r($dates);
    public function calculateWorkingDaysInMonth($year = '', $month = '')
    {
        //in case no values are passed to the function, use the current month and year
        if ($year == '') {
            $year = date('Y');
        }
        if ($month == '') {
            $month = date('m');
        }
        //create a start and an end datetime value based on the input year 
        $startdate = strtotime($year . '-' . $month . '-01');
        $enddate = strtotime('+' . (date('t', $startdate) - 1) . ' days', $startdate);
        $currentdate = $startdate;
        //get the total number of days in the month	
        $return = intval((date('t', $startdate)), 10);
        //loop through the dates, from the start date to the end date
        while ($currentdate <= $enddate) {
            //if you encounter a Saturday or Sunday, remove from the total days count
            if ((date('D', $currentdate) == 'Sun'))
            // if ((date('D',$currentdate) == 'Sat') || (date('D',$currentdate) == 'Sun'))
            {
                $return = $return - 1;
            }
            $currentdate = strtotime('+1 day', $currentdate);
        } //end date walk loop
        //return the number of working days
        return $return;
    }
    public function viewSalarySlip($empId)
    {
        // $wd=$this->calculateWorkingDaysInMonth(date('Y',strtotime('-1 months')),date('m',strtotime('-1 months')));
        //   $weekends=30-$wd; //Sunday Only
        // print_r($wd);die;
        $salary = GeneratedSalary::where('status', 1)->where('emp_id', $empId)->where('for_month_year', date('m-Y', strtotime('-1 months')))->with('empdetails', 'attendance')->first();
        $tbDed = TableDeduction::where('status', 1)->where('emp_id', $empId)->first();
        // print_r($salary->toArray());die;
        //   foreach($salary as $sl){
        //      $workingDays=7;
        //      $grossPay=$sl->basic_pay + $sl->da + $sl->hra + $sl->tpt + $sl->pers_pay + $sl->wa + $sl->misc;
        //      foreach($sl->attendance as $att){
        //         $workingDays++;
        //      }
        //      $sl->eol_d=(int)$this->getMonthlyDeductedSalary($grossPay,$workingDays,$weekends);

        //      $sl->misc_ded=$sl->misc_ded + (int)$this->getMonthlyDeductedSalary($grossPay,$workingDays,$weekends);
        //      $calulcatedSal=(int)$this->getMonthlySalary($grossPay,$workingDays+$weekends);
        //      $sl->calculatedSalary=$calulcatedSal;
        //   }
        // die;
        //   $employee = User::where('users.status', 1)
        //                  ->leftjoin('emp_salary_details','emp_salary_details.emp_id','=','users.emp_id')
        //                  ->leftjoin('emp_salary_deduction','emp_salary_deduction.emp_id','=','users.emp_id')
        //                  ->leftjoin('table_deduction','table_deduction.emp_id','=','users.emp_id')
        //                  ->select('users.*','emp_salary_deduction.*','table_deduction.*','table_deduction.misc as msc_tbl','emp_salary_details.*','users.emp_id as emp_id')
        //                  ->with('attendance')
        //                  ->get();
        //   $attenArray=[];
        //   if(!empty($employee) && $employee!=null){
        //      $attenArray=$employee->toArray();
        //   }

        //   print_r($wd);die;
        //   print_r($employee->toArray());die;
        // print_r(compact('employee'));die;
        return view('employee/salary_slip', compact('salary', 'tbDed'));
    }
    public function addSalEnt(Request $req)
    {
        $response['status'] = false;
        $response['message'] = 'Something went wrong';
        $data=[
            'hra_percent' => $req->input('hra_percent'),
            'da_percent' => $req->input('da_percent'),
            'tptValue' => $req->input('tptValue'),
            'tpt_amount' => $req->input('tpt_amount'),
        ];
        if (count(EmpSalPercentage::where($data)->get()) == 0) {
            if (EmpSalPercentage::insert($data)) {
                // Session::put('attendance',true);
                $response['status'] = true;
                $response['message'] = 'Details Added Successfully';
                return ($response);
            } else {
                $response['message'] = 'Failed to Add Salary Percentage Details';
                return ($response);
            }
        } 
        return response($response)->header('Content-Type', 'application/json');
    }
    public function generateSalaryTillNow(){
        $response['status'] = false;
        $response['message'] = 'Something went wrong';
        $currentFinancialYear = FinancialYear::where('current_status', 1)->first();
        // print_r($currentFinancialYear);die;
        $yearF = 0;
        if ($currentFinancialYear) {
            $yearF = $currentFinancialYear->financial_year;
        } else {
            $yearF = date('Y', strtotime('-1 years')) . date('Y');
        }
        $employee = User::where('users.status', 1)
            // ->where('attendace_date',)
            ->join('emp_salary_deduction', 'emp_salary_deduction.emp_id', '=', 'users.emp_id')
            ->join('emp_salary_details', 'emp_salary_details.emp_id', '=', 'users.emp_id')
            // ->leftjoin('emp_attendance','emp_attendance.emp_id','=','users.emp_id')
            ->select('users.emp_id', 'users.name', 'emp_salary_details.*', 'emp_salary_deduction.*')
            ->get();
       
        $month = date('m-Y');
        $temp = 0;
        foreach ($employee as $emp) {
            $genStatus = array(
                "emp_id" => $emp->emp_id,
                "for_month_year" => $month,
                "financial_year" => $yearF,
            );
            $leaves=$this->getEmpLeave($emp->emp_id);
            print_r($leaves->toArray());
            $re=$this->getAttendance($emp->emp_id);

            // print_r($genStatus);
            // if (count(SalaryGenStatus::where($genStatus)->get()) == 0) {
            //     if (SalaryGenStatus::insert($genStatus)) {
            //         // `emp_id`, `gen_sal_st_id`, ``, ``, ``, `da`, `hra`, `tpt`, `pers_pay`, `wa`, `misc`, 
            //         // `eol_h`, `eol_d`, `gpf`, `gpf_ref`, `cgeis`, `cghs`, `f_adv`, `rent_rec`, `misc_ded`, `tax_ded`
            //         // print_r($genStatus);
            //         // echo "\r\n";
            //         // print_r($emp->toArray());
            //         $id = 1;

            //         //Transaction 
            //         // enum('GPF', 'EOL', 'PERS_PAY', 'TPT', 'SALARY', 'LTC', 'CEA', 'MEDICAL', 'NPS', 'HRA', 'OVER_TIME', 'TA_DA', 'GRADE_PAY', 'TRANSPORT_ALLOWANCE')
            //         // TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->basic_pay, 'paid_as' => 'SALARY', 'initiated_for' => 'GRADE_PAY']);
            //         // TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->hra, 'paid_as' => 'SALARY', 'initiated_for' => 'HRA']);
            //         // TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->ta_da, 'paid_as' => 'SALARY', 'initiated_for' => 'TA_DA']);
            //         // TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->tpt, 'paid_as' => 'SALARY', 'initiated_for' => 'TPT']);
            //         // TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->pers_pay, 'paid_as' => 'SALARY', 'initiated_for' => 'PERS_PAY']);
            //         // TransactionDetails::insert(['financial_year' => $yearF, 'emp_id' => $emp->emp_id, 'amount' => $emp->gpf_ref, 'paid_as' => 'SALARY', 'initiated_for' => 'GPF']);


            //         $salaryArray = array(
            //             "emp_id" => $emp->emp_id,
            //             "gen_sal_st_id" => $id,
            //             "for_month_year" => $month,
            //             "financial_year" => $yearF,
            //             "basic_pay" => $emp->basic_pay,
            //             "da" => $emp->ta_da,
            //             "hra" => $emp->hra,
            //             "tpt" => $emp->tpt,
            //             "pers_pay" => $emp->pers_pay,
            //             "wa" => $emp->govt_perks,
            //             "misc" => 0,
            //             "eol_h" => 0,
            //             "eol_d" => 0,
            //             "gpf" => 0,
            //             "gpf_ref" => $emp->gpf_ref,

            //             "cgeis" => $emp->cgeis,
            //             "cghs" => $emp->cghs,
            //             "f_adv" => $emp->f_adv,
            //             "rent_rec" => $emp->rent_rec,
            //             "misc_ded" => $emp->misc,
            //             "tax_ded" => $emp->tax_deduction,
            //         );
            //         // print_r($salaryArray);
            //         // ;
            //         if (count(GeneratedSalary::where($salaryArray)->get()) == 0) {
            //             GeneratedSalary::insert($salaryArray);
            //             $temp = 1;
            //         }
            //     }
            // } else {
            //     // /echo 'Already Generated';
            //     $response['status'] = false;
            //     $response['message'] = 'Already Generated';
            // }
            // if ($temp == 1) {
            //     $response['status'] = true;
            //     $response['message'] = 'Salary Generated';
            // }
        }
    }
    public function getAttendance($empId){
        $condition=array('status'=>1,'emp_id'=>$empId);
        
        return EmployeeAttendanceModel::where($condition)->where('attendance_date','>', date('Y-m-01'))
        ->where('attendance_date','<',date('Y-m-01', strtotime('+1 months')))->get();
        
    }
    public function getEmpLeave($empId){
        $condition=array('status'=>1,'emp_id'=>$empId);
        
        $empLeaves=EmployeeLeave::where($condition)->where('from','>',date('Y-m-01'))
        ->where('to','<',date('Y-m-01', strtotime('+1 months')))->get();
        foreach($empLeaves as $lv){

        }
        
    }
}
