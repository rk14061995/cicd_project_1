<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CghsDeduction;

class EmployeeDeductions extends Controller
{
    //
    public function cgeisDeduction($id=null){
        $data = User::where('status', 1)->get();
        return view('employee/all_forms/emp_cgies_deduction',compact('data'));
    }
    public function employeeCghsDeductions($id=null){
        $condition=[];
        if($id!=null){
           $condition['emp_id']=$id;
            $data = User::where('status', 1)->where($condition)->with('cghsdeductions')->first();
        }else{
            $data = User::where('status', 1)->with('cghsdeductions')->get();
        }
       

        return view('employee/cghs_deduction',compact('data','id'));
    }
    public function employeeCgeisDeductions($id=null){
        $condition=[];
        if($id!=null){
            $condition['emp_id']=$id;
            $data = User::where('status', 1)->where($condition)->with('cgeisdeductions')->first();
        }else{
            $data = User::where('status', 1)->with('cgeisdeductions')->get();
        }
        
        return view('employee/cgeis_deduction',compact('data','id'));
    }
    public function employeeGpfDeductions($id=null){
        $condition=[];
        if($id!=null){
            $condition['emp_id']=$id;
            $data = User::where('status', 1)->where($condition)->with('gpfdeductions')->first();
        }else{
            $data = User::where('status', 1)->with('gpfdeductions')->get();
        }
        
        return view('employee/gpf_deduction',compact('data','id'));
    }
    public function employeeNpsDeductions($id=null){
        $condition=[];
        if($id!=null){
            $condition['emp_id']=$id;
            $data = User::where('status', 1)->where($condition)->with('npsdeductions')->first();
        }else{
            $data = User::where('status', 1)->where('do_joining','>',date('2004-m-d'))->with('npsdeductions')->get();
        }
        
        return view('employee/nps_deduction',compact('data','id'));
    }
    
}
