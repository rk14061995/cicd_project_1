<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DateTime;
use App\Models\User;
use App\Models\LeaveCategory;
use App\Models\EmployeeLeave;

class EmployeeLeaveController extends Controller
{
    //
    public function index($id = null)
    {
        // print_r($_REQUEST);
        
        $lvCategory = LeaveCategory::where('status', 1)->get();
        $condition = [];
        $empId=0;
        if (!empty($_REQUEST['emp']) && $_REQUEST['emp'] != null) {
            // die('dfdfdf');
            $empId=$_REQUEST['emp'];
            $condition['emp_id'] = $_REQUEST['emp'];
        }
        $data = User::where('status', 1)->get();
        $leave = EmployeeLeave::where('status', 1)->where($condition)->with('empdetails')->get();
        $dd=[];
        $category=[];
        
        foreach($lvCategory as $cate){
            if(!empty($empId)){
                $lvs=EmployeeLeave::getEmpLeaveDetails($cate->category_name,$empId);
                $totalLeaves=0;
                foreach ($lvs as $lv) {
                    $time1 = new DateTime($lv->from);
                    $time2 = new DateTime($lv->to);
                    $interval = $time1->diff($time2);
                    $days = $interval->format('%d');
                    $totalLeaves+=$days+1;
                
                }
                $category[]=array(
                    "category"=>$cate->category_name,
                    "days"=>$cate->days_count,
                    "employeeLeaves"=> $totalLeaves
                );
            }else{
                $category[]=array(
                    "category"=>$cate->category_name,
                    "days"=>$cate->days_count,
                    "employeeLeaves"=> 0
                );
            }
        }
        // die;
        // print_r($category);die; 
        return view('employee/leave_rel', compact('empId','data', 'lvCategory', 'leave','category'));
    }
}
