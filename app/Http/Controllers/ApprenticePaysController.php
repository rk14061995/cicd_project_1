<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApprenticePaysModel;

class ApprenticePaysController extends Controller
{
    //
    public function addnewbill(Request $req)
    {
        $response['status'] = false;
        $response['message'] = 'Something went wrong';
        $data = array(
            'reg_no' => $req->input('reg_no'),
            'name' => $req->input('name'),
            'fname' => $req->input('fname'),
            'dob' => $req->input('dob'),
            'trade' => $req->input('trade'),
            'doa' => $req->input('doa'),
            'pdays' => $req->input('pdays'),
            'day_in_month' => $req->input('dmonth'),
            'stipend' => $req->input('stpnd'),
            'net_pay' => $req->input('nt_py'),
            'remarks' => $req->input('rem'),
            'month' => date('m'),
            'year' => date('Y'),
        );
        $res = ApprenticePaysModel::where($data)->get();
        if (count($res) > 0) {
            $response['status'] = true;
            $response['message'] = 'Bill Details Already Exists';
            return ($response);
        } else {
            if (ApprenticePaysModel::insert($data)) {
                // Session::put('attendance',true);
                $response['status'] = true;
                $response['message'] = 'Bill Added Successfully';
                return ($response);
            } else {
                $response['message'] = 'Failed to Add Bill';
                return ($response);
            }
            return response($response)->header('Content-Type', 'application/json');
        }
        return response($response)->header('Content-Type', 'application/json');
    }
}
