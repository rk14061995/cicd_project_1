<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TransactionDetails;

class Transactions extends Controller
{
    //
    public function transaction(){
        $TransactionDetails = TransactionDetails::where('status', 1)->where('emp_id','<>',null)->get();
        // print_r($TransactionDetails->toArray());die;
        return view("employee.transaction", compact('TransactionDetails'));
    }
}
